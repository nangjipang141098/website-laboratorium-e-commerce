

<?php 
 include "koneksi.php";
	
 if(!isset($_SESSION)) 
 { 
     session_start(); 
 } 

 ?>

<center><h2>TAMBAH DATA MATA KULIAH</h2></center>
<br>

<!-- FORM -->
<form method="post" enctype="multipart/form-data" autocomplete="off">
	<div class="container">
		
		<div class="form-group row">
			<label for="harga_beli" class="col-sm-2 col-form-label">KODE HARI</label>
			<div class="col-sm-8">	
				<input type="text" class="form-control" name="kode_matkul" placeholder="KODE MATA KULIAH" required>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> MATA KULIAH </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="matkul" placeholder="MATA KULIAH YANG ADA DI LABORATORIUM"  required>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> HARI </label>
			<div class="col-sm-8">
                <input type="radio" name="hari" value="SENIN" > SENIN
                <input type="radio" name="hari" value="SELASA" > SELASA 
                <input type="radio" name="hari" value="RABU"  > RABU <br>
                <input type="radio" name="hari" value="KAMIS" > KAMIS 
                <input type="radio" name="hari" value="JUMAT"  > JUMAT
                <input type="radio" name="hari" value="SABTU" > SABTU 
			</div>
		</div>
        <div class="form-group row">
                <label class="col-sm-2 col-form-label"> JAM </label>
                <div class="col-sm-8">	
                <select name="jam" class="form-control" required>
                    <option selected disabled>....</option>
					<option value="08.00-09.40"> 08.00-09.40 </option>
					<option value="09.50-11.30"> 09.50-11.30 </option>
					<option value="11.40-13.20"> 11.40-13.20 </option>
					<option value="13.30-15.10"> 13.30-15.10 </option>
					<option value="15.20-17.00"> 15.20-17.00 </option>
					<option value="17.00-18.40"> 17.00-18.40 </option>
					<option value="18.50-20.20"> 18.50-20.20 </option>
					<option value="20.20-22.00"> 20.20-22.00 </option>
			    </select>
                </div>
        </div>	
       
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> DOSEN </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="dosen" placeholder="NAMA DOSEN YANG MENGAJAR MATA KULIAH"  required>
			</div>
		</div>
		<div class="form-group row">
                <label class="col-sm-2 col-form-label"> TAHUN AJARAN </label>
                <div class="col-sm-8">	
                <select name="tahun_ajaran" class="form-control" required>
                    <option selected disabled>....</option>
					<option value="2019-2020"> 2019-2020 </option>
					<option value="2020-2021"> 2020-2021 </option>
					<option value="2021-2022"> 2021-2022 </option>
					<option value="2022-2023"> 2022-2023 </option>
					<option value="2023-2024"> 2023-2024 </option>
					<option value="2024-2025"> 2024-2025 </option>
			    </select>
                </div>
        </div>	
        
		<div class="form-group row">
		<div class="col-sm-10" style="float: right;">	
				<button class="btn btn-lg btn-danger" name="batal">BATAL</button>
				<button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
		</div>	
		</div>
	</div>
</form>

	<?php 
	
	
	if (isset($_POST['tambah'])) 
	{
			
			$kode_matkul = $_POST['kode_matkul'];
			$matkul = $_POST['matkul'];
			$hari = $_POST['hari'];
			$jam = $_POST['jam'];
			
			$dosen = $_POST['dosen'];
			$tahun_ajaran = $_POST['tahun_ajaran'];
			date_default_timezone_set('Asia/Jakarta');  
			$lu =  date("l, j F Y, H:i")  ;
		
			$cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT matkul FROM matkul WHERE kode_matkul='$kode_matkul'"));

			if($cek > 0){		
				while ($row = mysqli_fetch_array(mysqli_query($koneksi, "SELECT matkul FROM matkul WHERE kode_matkul='$kode_matkul'"))) {
					echo "<script>alert('KODE SUDAH DIGUNAKAN ".$row['matkul']." HARI ".$row['hari']."')</script>";  
					echo "<script>location='index.php?halaman=tambah_matkul';</script> ";
				}
			}
			else {
				$kon = mysqli_query($koneksi, "INSERT INTO matkul 
                    (kode_matkul, matkul, hari, jam, dosen, tahun_ajaran)
                    VALUES ('$kode_matkul','$matkul','$hari','$jam','$dosen','$tahun_ajaran')
					");
					echo "<script>alert('DATA MATA KULIAH TELAH DITAMBAHKAN');</script>";
					echo "<script>location='index.php?halaman=daftar_matkul';</script> ";
			}
            
            
           

		
	
	}

	?>