<?php include "koneksi.php"; ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
				<!-- TABLE DEKORASI -->
				<div class="table-responsive">	
					<table class='table table-bordered'>
						<tr>
							<td>
								<form method="post" enctype="multipart/form-data" autocomplete="off">
									<div class="form-group">
										<label> BERITA ACARA PERKULIAHAN TAHUNAN </label>
										<select name="kategori">
											<option selected disabled>....</option>
											<option value="1"> 2019</option>
											<option value="2"> 2020 </option>
											<option value="3"> 2021 </option>
                                            <option value="4"> 2022 </option>
                                            <option value="5"> 2023 </option>
                                            <option value="6"> 2024 </option>
										</select>
										<button class="btn btn-info muted" name="ubah">CARI</button>
                                        <br><label> LABORATORIUM E-COMMERCE </label>
                                        <br><label> FAKULTAS TEKNOLOGI KOMUNIKASI DAN INFORMATIKA </label>
                                        <br><label> UNIVERSITAS NASIONAL </label>
								</form>
							</td>	
						<tr>
					</table>
				</div>
				<a href="index.php?halaman=tambah_data_berita_acara" class="btn btn-primary btn-lg btn-block">Tambah Data Berita Acara</a>
				<div class="table-responsive">	
					<table class='table table-bordered'>
						<thead>
							<tr>
							<th>Mata Kuliah</th>
							<th>Dosen</th>
							<th>Tahun Ajaran</th>
							<th>Dokumen</th>
							<th>Aksi</th>					
							</tr>
						</thead>
						<tbody>
							<?php
								if (isset($_POST['reload'])) 
								{
									echo "<script>location='index.php?halaman=surat_keluar';</script> ";
								}
							?>
							<?php 
							if (isset($_POST['ubah'])) 
							{
								switch($_POST['kategori']){
                                    case 1:
                                        $n='2019-2020';
										$rs = mysqli_query($koneksi, "SELECT * FROM perkuliahan WHERE tahun_ajaran='$n'");
									break;
                                    case 2:
                                        $n='2020-2021';
                                        $rs = mysqli_query($koneksi, "SELECT * FROM perkuliahan WHERE tahun_ajaran='$n'");
									break;
                                    case 3:
                                        $n='2021-2022';	
                                        $rs = mysqli_query($koneksi, "SELECT * FROM perkuliahan WHERE tahun_ajaran='$n'");
                                    break;
                                    case 4:	
                                        $n='2022-2023';
                                        $rs = mysqli_query($koneksi, "SELECT * FROM perkuliahan WHERE tahun_ajaran='$n'");
									break;
                                    case 5:
                                        $n='2023-2024';
                                        $rs = mysqli_query($koneksi, "SELECT * FROM perkuliahan WHERE tahun_ajaran='$n'");
									break;
                                    case 6:	
                                        $n='2024-2025';
                                        $rs = mysqli_query($koneksi, "SELECT * FROM perkuliahan WHERE tahun_ajaran='$n'");
									break;
								}
										
							}
							
							else{
						 		$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
						 		$perpage = 7;
								$limit = ($page - 1) * $perpage;
								$prev = 1;
								$next = 2;
								$start_page = ($page - $prev) < 1 ? 1 : ($page - $prev);
								$sql = 'SELECT * FROM perkuliahan';
								$rs = mysqli_query($koneksi, $sql);
								$record = mysqli_num_rows($rs);
								$total_page = ceil($record / $perpage);
								$display_page = $start_page + $prev + $next;
								if($display_page > $total_page){
								$display_page = $total_page;
								}
								$sql .= ' LIMIT '.$limit.','.$perpage;
								$rs = mysqli_query($koneksi, $sql);
								}
							 ?>
							
							<?php while($data = mysqli_fetch_assoc($rs)){ ?>
								<tr>
								<td width="50"><?php echo $data['matkul'] ; ?></td>
								<td width="50"><?php echo $data['dosen'] ; ?></td>
								<td width="50"><?php echo $data['tahun_ajaran'] ; ?></td>
								<td width="60"><img src="../surat/berita_acara/<?php echo $data['dokumen'] ;?>" width="90"></td>
								<td  width="50">
									<a href="index.php?halaman=hapus_data_surat_keluar&ni=<?php echo $data['nomer'] ; ?> " class="btn btn-danger" onClick="return confirm('Apakah anda yakin data ini akan di hapus secara permanen?');">Hapus</a>
									<a href="index.php?halaman=ubah_data_surat_keluar&ni=<?php echo $data['nomer'] ; ?>" class="btn btn-warning">Edit</a>
									<a href="index.php?halaman=view_data_surat_keluar&ni=<?php echo $data['nomer'] ; ?>" class="btn btn-info">View</a>
								</td>
								</tr>
							<?php } ?>
							
						</tbody>
					</table>
				</div>
				<?php
				  	$paging = null;
					if($total_page > 1){
					   	$paging .= '<ul class="pagination">';
				  			if($page > ($prev + 1)){
				   				$paging .= '<li><a href="index.php?halaman=berita_acara&page=1">first</a></li>';
				    			$paging .= '<li><a href="index.php?halaman=berita_acara&page='.($page - 1).'">prev</a></li>';
				  			}	
							for($i=$start_page; $i<=$display_page; $i++){
								if($i == $page){
									$paging .= '<li><a href="#'.$i.'">'.$i.'</a></li>';
								}else{
									$paging .= '<li><a href="index.php?halaman=berita_acara&page='.$i.'">'.$i.'</a></li>';
								}
							}
							if($total_page > $display_page){
								$paging .= '<li><a href="index.php?halaman=berita_acara&page='.($page + 1).'">next</a></li>';
								$paging .= '<li><a href="index.php?halaman=berita_acara&page='.$total_page.'">last</a></li>';
							}
				   			$paging .= '<ul>';
				  }
				 echo $paging;
				 ?>
            </div>
        
	</body>
</html>
