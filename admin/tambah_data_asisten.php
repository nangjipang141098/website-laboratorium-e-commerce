<center><h2>TAMBAH DATA ASISTEN LABORATORIUM E-COMMERCE</h2></center>
<br>


<?php 
 include "koneksi.php";

 ?>


<!-- FORM -->
<form method="post" enctype="multipart/form-data" autocomplete="off">
	<div class="container">
		<div class="form-group row">
			<label class="col-sm-2 col-form-label"> NAMA </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="nama" placeholder="NAMA LENGKAP"  required>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label"> NPM </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="npm" placeholder="NPM"  required>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label"> PASSWORD </label>
			<div class="col-sm-8">	
				<input type="text" class="form-control " name="password" placeholder="KOMBINASIKAN ANGKA DAN HURUF"  required>
			</div>
		</div>
		<div class="form-group row">
			<label for="qtty" class="col-sm-2 col-form-label">PRODI</label>
			<div class="col-sm-8">	
				<input type="radio" name="prodi" value="TI"  required> TEKNIK INFORMATIKA
                <input type="radio" name="prodi" value="SI" > SISTEM INFORMASI </td>
			</div>
		</div>
		<div class="form-group row">
			<label for="harga_beli" class="col-sm-2 col-form-label">TAHUN MASUK</label>
			<div class="col-sm-8">	
				<input type="number" class="form-control" name="tahun_masuk" placeholder="TAHUN MENJADI ASISTEN LAB E-COMMERCE" required>
			</div>
		</div>
		<div class="form-group row">
			<label  class="col-sm-2 col-form-label">FOTO</label>
			<div class="form-group col-sm-8">	
				<input type="file" name="foto" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 col-form-label">ABOUT</label>
			<textarea class="form-control" rows="5"></textarea>
			</div>
		<div class="form-group row">
		<div class="col-sm-10" style="float: right;">	
				<button class="btn btn-lg btn-danger" name="batal">BATAL</button>
				<button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
		</div>	
		</div>
	</div>
</form>

	<?php 
	include "koneksi.php";
	
	if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
	
	if (isset($_POST['tambah'])) 
	{
			$namafoto=$_FILES['foto'] ['name'];
			$lokasifoto =$_FILES['foto'] ['tmp_name'];
			$npm = $_POST['npm'];
			$password = $_POST['password'];
			$nama = $_POST['nama'];
			$prodi = $_POST['prodi'];
			$tahun_masuk = $_POST['tahun_masuk'];
			$about = $_POST['about'];
			date_default_timezone_set('Asia/Jakarta');  
			$lu =  date("l, j F Y, H:i")  ;
		
			move_uploaded_file($lokasifoto, "../surat/foto_asisten/$namafoto");

            $kon = mysqli_query($koneksi, "INSERT INTO admin 
					(npm, password, nama, prodi, tahun_masuk, about, foto) VALUES
					('$npm', '$password', '$nama', '$prodi', '$tahun_masuk', '$about', '$namafoto');
					");
            
           

		echo "<script>alert('DATA ASISTEN TELAH DITAMBAHKAN');</script>";
		echo "<script>location='index.php?halaman=akses';</script> ";
	
	}

	?>