<?php 
	include "koneksi.php"; 
?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Data Logistik Pandamanda</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="shortcut icon" href="assets/img/favicon.ico">
   	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<!-- TOP NAV -->
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Pandamanda</a> 
            </div>
            <div style="color: white; padding: 15px 20px 5px 20px; float: left; font-size: 20px; width: 670px;">
			<?php  date_default_timezone_set('Asia/Jakarta'); echo  date("l, j F Y, H:i");  ?> &nbsp; &nbsp; &nbsp; &nbsp;
            </div>
		  
	    </nav>   
   		<!-- SIDE NAV -->
	    <nav class="navbar-default navbar-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav" id="main-menu">				
					<li><a href="view_rekruitment.php"><i class="fa fa-home fa-3x"></i> Isi Formulir </a></li>
					<li><a href="view_hasil.php"><i class="fa fa-diamond fa-3x"></i>Hasil Tes</a></li>
					<li><a href="view_pengumuman.php"><i class="fa fa-diamond fa-3x"></i>Kriteria</a></li>
					<li><a href="login.php"><i class="fa fa-user fa-3x"></i> Back</a></li>		
				</ul>
			</div>     
	    </nav>
	    <!-- PAGE INNER  -->
	    <div id="page-wrapper" >
	        <div id="page-inner">    


   
<!-- FORM -->
<form method="post" enctype="multipart/form-data" autocomplete="off">
<h2> SILAHKAN ISI FORM PENDAFTARAN BERIKUT </h2>
	<div class="container">
		<div class="form-group row">
			<label class="col-sm-2 col-form-label"> NAMA </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="nama" placeholder="NAMA LENGKAP"  required>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label"> NPM </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="npm" placeholder="NPM"  required>
			</div>
		</div>
		<div class="form-group row">
			<label for="qtty" class="col-sm-2 col-form-label">PRODI</label>
			<div class="col-sm-8">	
				<input type="radio" name="prodi" value="TI"  required> TEKNIK INFORMATIKA
                <input type="radio" name="prodi" value="SI" > SISTEM INFORMASI </td>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> APAKAH ANDA BISA PROGRAMING? BAHASA PROGRAM APA YANG DIKUASAI? DESKRIPSIKAN SECARA SINGKAT </label>
			<div class="col-sm-8">
            <textarea class="form-control" rows="5" name="coding"></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> APAKAH ANDA BISA NETWORKING? SEJAUH MANA PENGETAHUAN ANDA TENTANG JARINGAN? DESKRIPSIKAN SECARA SINGKAT </label>
			<div class="col-sm-8">
            <textarea class="form-control" rows="5" name="jarkom"></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> APAKAH ANDA BISA DESIGN? SOFTWARE APA YANG BIASA DIGUNAKAN? DESKRIPSIKAN SECARA SINGKAT </label>
			<div class="col-sm-8">
            <textarea class="form-control" rows="5" name="design"></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> ALASAN MENDAFTAR MENJADI ASISTEN LAB E-COMMERCE </label>
			<div class="col-sm-8">
            <textarea class="form-control" rows="5" name="alasan"></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> DESKRIPSIKAN TENTANG ANDA </label>
			<div class="col-sm-8">
            <textarea class="form-control" rows="5" name="about"></textarea>
			</div>
		</div>
		<div class="form-group row">
			<label  class="col-sm-2 col-form-label">FILE (FORMAT JPG)</label>         
			<div class="form-group col-sm-8">	
				<label >FOTO, CV, KTM, TRANSKIP (DIJADIKAN SATU) DENGAN FORMAT WAJIB PDF, UKURAN MAX 500 KB, JANGAN BLUR</label>  
				<input type="file" name="foto" class="form-control">
			</div>
		</div>
		<div class="form-group row">
		<div class="col-sm-10" style="float: right;">	
				<button class="btn btn-lg btn-danger" name="batal">BATAL</button>
				<button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
		</div>	
		</div>
	</div>
</form>

	<?php 
	include "koneksi.php";
	
	
	if (isset($_POST['tambah'])) 
	{
			$namafoto=$_FILES['foto'] ['name'];
			$lokasifoto =$_FILES['foto'] ['tmp_name'];
			$npm = $_POST['npm'];
			$nama = $_POST['nama'];
			$prodi = $_POST['prodi'];
            $about = $_POST['about'];
            $network = $_POST['jarkom'];
            $design = $_POST['design'];
            $coding = $_POST['coding'];
            $alasan = $_POST['alasan'];
            $keterangan = "LOLOS";
			date_default_timezone_set('Asia/Jakarta');  
			$lu =  date("l, j F Y, H:i")  ;
			$tahun = getdate();
			$n = $tahun['year'];
			move_uploaded_file($lokasifoto, "../surat/rekruitment/$namafoto");
      
         
            $kon = mysqli_query($koneksi, "INSERT INTO requitment
            (npm, nama, prodi, programing, design, networking, cv, alasan, about, lolos, tahun) 
            VALUES 
            ('$npm','$nama','$prodi','$coding','$design','$network','$namafoto', '$alasan','$about','$keterangan', '$n')
			");
            
           

		echo "<script>alert('ANDA TELAH MENDAFTAE SEBAGAI CALON ASLAB E-COMMERCE');</script>";
		echo "<script>location='view_rekruitment.php';</script> ";
	
	}

	?>


    
</div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>	
	</body>
</html>
