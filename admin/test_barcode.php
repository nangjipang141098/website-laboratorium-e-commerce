<!DOCTYPE html>

<form action="" method="post">
    <head>
        <meta charset="UTF-8">
        <title>BERITA ACARA</title>
        <link rel="shortcut icon" href="assets/img/logo.jpg">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        
    </head>
    <body>
       
        <div class="container" id="QR-Code">
            <div class="panel-warning">
                <div class="panel-heading">
                    <div class="navbar-form navbar-header">
                        <h4>SCAN BARCODE MATKUL</h4>
                    </div>
                    <div class="navbar-form navbar-left">
                        <select class="form-control" id="camera-select"></select>
                        <div class="form-group">
                            <input id="image-url" type="text" class="form-control" placeholder="Image url">
                            <button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
                            <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>
                            <button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span> OPEN CAMERA </button>
                            <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span> PAUSE </button>
                            <button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span> CLOSE CAMERA </button>
                            <a href="index.php?halaman=daftar_matkul" class="btn btn-info btn-sm">BACK</a>
                         </div>
                    </div>
                </div>
                <div class="panel-body text-center">Quick Response Code
                    <div class="col-md-6">KAMERA<br>
                        <div class="well" style="position: relative;display: inline-block;">
                            <canvas width="320" height="240" id="webcodecam-canvas"></canvas>
                            <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="thumbnail" id="result">
                            <div class="well" style="overflow: hidden;">
                                <img width="320" height="240" id="scanned-img" src=""> 
                            </div>
                         
                        </div>
                    </div>
                   <div class="col-md-20">
                        <div class="thumbnail" id="result">
                                <textarea id="scanned-QR" class="form-control" rows="5" name="nama"></textarea>
                                <input type="submit" name="submit" class="btn btn-primary btn-lg btn-block"></input>     
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                
                </div>
            </div>
        </div>
        <script type="text/javascript"  src="js/filereader.js"></script>
        <!-- Using jquery version: -->
        <!--
            <script type="text/javascript" src="js/jquery.js"></script>
            <script type="text/javascript" src="js/qrcodelib.js"></script>
            <script type="text/javascript" src="js/webcodecamjquery.js"></script>
            <script type="text/javascript" src="js/mainjquery.js"></script>
        -->
        <script type="text/javascript" src="js/qrcodelib.js"> var video = document.querySelector("#video-webcam");

// minta izin user
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

// jika user memberikan izin
if (navigator.getUserMedia) {
    // jalankan fungsi handleVideo, dan videoError jika izin ditolak
    navigator.getUserMedia({ video: true }, handleVideo, videoError);
}

// fungsi ini akan dieksekusi jika  izin telah diberikan
function handleVideo(stream) {
    video.srcObject = stream;
}

// fungsi ini akan dieksekusi kalau user menolak izin
function videoError(e) {
    // do something
    alert("Izinkan menggunakan webcam untuk demo!")
}</script>
        <script type="text/javascript" src="js/webcodecamjs.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
    </body>
    </form>
</html>

<?php
if(isset($_POST['submit'])){
    include "koneksi.php";
    $no = $_GET['ni'];
    $nama = $_POST['nama'];
    date_default_timezone_set('Asia/Jakarta');  
    $lu =  date("l, j F Y, H:i");
    
    $kon = mysqli_query($koneksi, "INSERT INTO berita_acara(nama, tanggal, kode_matkul) VALUES ('$nama','$lu', '$no')");
    echo "<script>alert('DATA MATA KULIAH HARI INI TELAH DI SCAN');</script>";
}

?>