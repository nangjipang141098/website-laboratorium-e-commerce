<center><h2>TAMBAH DATA SURAT KELUAR LABORATORIUM E-COMMERCE</h2></center>
<br>


<?php 
 include "koneksi.php";
 ?>


<!-- FORM -->
<form method="post" enctype="multipart/form-data" autocomplete="off">
	<div class="container">
		
		<div class="form-group row">
			<label for="harga_beli" class="col-sm-2 col-form-label">NOMER</label>
			<div class="col-sm-8">	
				<input type="number" class="form-control" name="nomer" placeholder="NOMER LAMPIRAN PADA SURAT KELUAR LAB E-COMMERCE" required>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> PERIHAL </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="perihal" placeholder="PERIHAL YANG TERDAPAT PADA SURAT KELUAR LAB E-COMMERCE"  required>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> DARI </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="dari" placeholder="KEPADA SIAPA SURAT LAB E-COMMERCE DIBERIKAN"  required>
			</div>
		</div>
		<div class="form-group row">
			<label  class="col-sm-2 col-form-label">FILE (FORMAT PDF)</label>         
			<div class="form-group col-sm-8">	
				<input type="file" name="foto" class="form-control">
			</div>
		</div>
		<div class="form-group row">
		<div class="col-sm-10" style="float: right;">	
				<button class="btn btn-lg btn-danger" name="batal">BATAL</button>
				<button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
		</div>	
		</div>
	</div>
</form>

	<?php 
	include "koneksi.php";
	
	if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
	
	if (isset($_POST['tambah'])) 
	{
			$namafoto=$_FILES['foto'] ['name'];
			$lokasifoto =$_FILES['foto'] ['tmp_name'];
			$nomer = $_POST['nomer'];
			$perihal = $_POST['perihal'];
			$dari = $_POST['dari'];
			date_default_timezone_set('Asia/Jakarta');  
			$lu =  date("l, j F Y, H:i")  ;
		
			move_uploaded_file($lokasifoto, "../surat/keluar/$namafoto");

            $kon = mysqli_query($koneksi, "INSERT INTO surat_keluar 
					(nomer, perihal, tujuan, dokumen) VALUES
					('$nomer', '$perihal', '$dari', '$namafoto');
					");
            
           

		echo "<script>alert('DATA SURAT KELUAR TELAH DITAMBAHKAN');</script>";
		echo "<script>location='index.php?halaman=surat_keluar';</script> ";
	
	}

	?>