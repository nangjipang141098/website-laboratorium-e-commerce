<?php include "koneksi.php"; ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
				<!-- TABLE DEKORASI -->
				<div class="table-responsive">	
					<table class='table table-bordered'>
						<tr>
							<td>
								<form method="post" enctype="multipart/form-data" autocomplete="off">
									<div class="form-group">
										<label> STRUKTUR ORGANISASI LABORATORIUM E-COMMERCE TAHUN </label>
										<select name="kategori">
											<option selected disabled>....</option>
											<option value="1"> 2019</option>
											<option value="2"> 2020 </option>
											<option value="3"> 2021 </option>
                                            <option value="4"> 2022 </option>
                                            <option value="5"> 2023 </option>
                                            <option value="6"> 2024 </option>
										</select>
										<button class="btn btn-info muted" name="ubah">CARI</button>
                                        <br><label> FAKULTAS TEKNOLOGI KOMUNIKASI DAN INFORMATIKA </label>
                                        <br><label> UNIVERSITAS NASIONAL </label>
								</form>
							</td>	
						<tr>
					</table>
				</div>
			
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                <a href="index.php?halaman=tambah_struktur" class="btn btn-info ">Tambah Struktur </a>
                
                <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Hapus Struktur
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="index.php?halaman=ubah_struktur&ni=2019" class="btn btn-outline-primary">2019</a>
                    <a href="index.php?halaman=ubah_struktur&ni=2020" class="btn btn-outline-primary">2020</a>
                    <a href="index.php?halaman=ubah_struktur&ni=2021" class="btn btn-outline-primary">2021</a>
                    <a href="index.php?halaman=ubah_struktur&ni=2022" class="btn btn-outline-primary">2022</a>
                    <a href="index.php?halaman=ubah_struktur&ni=2023" class="btn btn-outline-primary">2023</a>
                    <a href="index.php?halaman=ubah_struktur&ni=2024" class="btn btn-outline-primary">2024</a>
                    </div>
                </div>
                </div>
                <br>
						<tbody>
							<?php
								if (isset($_POST['reload'])) 
								{
									echo "<script>location='index.php?halaman=akses';</script> ";
								}
							?>
							<?php 
							if (isset($_POST['ubah'])) 
							{
								switch($_POST['kategori']){
                                    case 1:	
                                        $n=2019;
										$rs = mysqli_query($koneksi, "SELECT * FROM struktur WHERE tahun_jabatan='$n'");
									break;
                                    case 2:
                                        $n=2020;	
										$rs = mysqli_query($koneksi, "SELECT * FROM struktur WHERE tahun_jabatan='$n'");
									break;
                                    case 3:	
                                        $n=2021;		
										$rs = mysqli_query($koneksi, "SELECT * FROM struktur WHERE tahun_jabatan='$n'");
                                    break;
                                    case 4:	
                                        $n=2022;
										$rs = mysqli_query($koneksi, "SELECT * FROM struktur WHERE tahun_jabatan='$n'");
									break;
                                    case 5:
                                        $n=2023;	
										$rs = mysqli_query($koneksi, "SELECT * FROM struktur WHERE tahun_jabatan='$n'");
									break;
                                    case 6:	
                                        $n=2024;		
										$rs = mysqli_query($koneksi, "SELECT * FROM struktur WHERE tahun_jabatan='$n'");
									break;
								}
										
							}
							
							?>
							
                            <?php
                            if (empty($rs)){
                                echo "<center><h4> SILAHKAN PILIH TAHUN PRIODE </h4></center>";
                            }
                           
                            else{ 
                                echo "<pre>"; 
                                 while($data = mysqli_fetch_assoc($rs)){ 
                                    if($data['jabatan']=='KEPALA LABORATORIUM'){
                                        while($d = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM struktur WHERE jabatan='KEPALA LABORATORIUM'"))){
                                            echo "<b>" . "KEPALA LABORATORIUM : <pre>" . $d['nama'] . "</pre><br>";
                                            break;
                                        }
                                     } 
                                     else if($data['jabatan']=='KORDINATOR LABORATORIUM'){
                                        while($d = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM struktur WHERE jabatan='KORDINATOR LABORATORIUM'"))){
                                            echo "<b>" . "KORDINATOR LABORATORIUM : <pre>" . $d['nama'] . "</pre><br>";
                                            break;
                                        }
                                     } 
                                     else if($data['jabatan']=='SEKRETARIS LABORATORIUM'){
                                        while($d = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM struktur WHERE jabatan='SEKRETARIS LABORATORIUM'"))){
                                            echo "SEKRETARIS LABORATORIUM : <pre>" . $d['nama'] . "</pre><br>";
                                            break;
                                        }
                                     }
                                     else if($data['jabatan']=='PENJADWALAN LABORATORIUM'){
                                        while($d = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM struktur WHERE jabatan='PENJADWALAN LABORATORIUM'"))){
                                            echo "PENJADWALAN LABORATORIUM : <pre>" . $d['nama'] . "</pre><br>";
                                            break;
                                        }
                                     }
                                     else if($data['jabatan']=='HUMAS I LABORATORIUM'){
                                        while($d = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM struktur WHERE jabatan='HUMAS I LABORATORIUM'"))){
                                            echo "HUMAS I LABORATORIUM : <pre>" . $d['nama'] . "</pre><br>";
                                            break;
                                        }
                                     }
                                     else if($data['jabatan']=='HUMAS II LABORATORIUM'){
                                        while($d = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM struktur WHERE jabatan='HUMAS II LABORATORIUM'"))){
                                            echo "HUMAS II LABORATORIUM : <pre>" . $d['nama'] . "</pre><br>";
                                            break;
                                        }
                                     }
                                     else if($data['jabatan']=='PENGEMBANGAN I LABORATORIUM'){
                                        while($d = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM struktur WHERE jabatan='PENGEMBANGAN I LABORATORIUM'"))){
                                            echo "PENGEMBANGAN I LABORATORIUM : <pre>" . $d['nama'] . "</pre><br>";
                                            break;
                                        }
                                     }
                                     else if($data['jabatan']=='PENGEMBANGAN II LABORATORIUM'){
                                        while($d = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM struktur WHERE jabatan='PENGEMBANGAN II LABORATORIUM'"))){
                                            echo "PENGEMBANGAN II LABORATORIUM : <pre>" . $d['nama'] . "</pre><br>";
                                            break;
                                        }
                                     }
                                     
                                    } 
                                    echo "</pre>";
                                }       
                         
                         ?>
                    <center>
                    
                    
                
                    
                </div>
                </center> 
                           
				
            </div>
        
	</body>
</html>
