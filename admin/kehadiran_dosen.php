<?php
//library phpqrcode
include "phpqrcode/qrlib.php";
include 'koneksi.php';
 
//direktory tempat menyimpan hasil generate qrcode jika folder belum dibuat maka secara otomatis akan membuat terlebih dahulu
$tempdir = "temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);
 
?>
<html>
<head>
</head>
<body>
 <br><br><br><br><br>
  <h2><center><b>BERITA ACARA MATA KULIAH</b></h2></center>
  <center><button onclick="window.print()">Cetak Halaman Web</button></center><br>
    <table class="table table-striped table-dark" border="3" align="center">
        <thead>
            <th><center>HARI<center></th>
            <th><center>JAM<center></th>
            <th><center>MATKUL<center></th>
            <th><center>DOSEN</center></th>
            <th><center>TAHUN AJARAN<center></th>
        </thead>
        <tbody>
        <?php
            $no = $_GET['ni'];	
            $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
            $perpage = 8;
           $limit = ($page - 1) * $perpage;
           $prev = 1;
           $next = 2;
           $start_page = ($page - $prev) < 1 ? 1 : ($page - $prev);
           $sql = "SELECT hari, jam, matkul, dosen, tanggal, tahun_ajaran FROM matkul INNER JOIN berita_acara USING (kode_matkul) WHERE kode_matkul='$no' AND tahun_ajaran='2019-2020' ORDER BY jam AND hari ASC";
           $rs = mysqli_query($koneksi, $sql);
           $record = mysqli_num_rows($rs);
           $total_page = ceil($record / $perpage);
           $display_page = $start_page + $prev + $next;
           if($display_page > $total_page){
           $display_page = $total_page;
           }
           $sql .= ' LIMIT '.$limit.','.$perpage;
           $rs = mysqli_query($koneksi, $sql);
           

            while ($row = mysqli_fetch_assoc($rs)) {
        ?>
            <tr>
                <td><center><pre><?php echo $row['hari']; ?></pre></center></td>
                <td><center><pre><?php echo $row['jam']; ?></pre></center></td>
                <td><center><pre><?php echo $row['matkul']; ?></pre></center></td>
                <td><center><pre><?php echo $row['dosen']; ?></pre></center></td>       
                <td><center><pre><?php echo $row['tahun_ajaran']; ?></pre></center></td>       
            </tr>
        <?php } ?>

        </tbody>
    </table>

    <table class="table table-striped table-dark" border="3" align="center">
        <thead>
        <br>
            <th><center>PERWAKILAN ASISTEN<center></th>
            <th><center>DOSEN PENGAJAR MATA KULIAH<center></th>
        </thead>
        <tbody>
        <?php
           $sql2 = "SELECT dosen
                    FROM matkul WHERE tahun_ajaran='2019-2020' AND kode_matkul='$no'";
           $rs2 = mysqli_query($koneksi, $sql2);
           $rs2 = mysqli_query($koneksi, $sql2);
           

            while ($row2 = mysqli_fetch_assoc($rs2)) {
        ?>
            <tr>
                <td width="355" height="100"><center></center></td>
                <td width="415"><center><br><br><?php echo $row2['dosen']; ?></center></td>         
            </tr>
        <?php } ?>

        </tbody>
    </table>



             <?php
				  	$paging = null;
					if($total_page > 1){
					   	$paging .= '<ul class="pagination">';
				  			if($page > ($prev + 1)){
				   				$paging .= '<li><a href="kehadiran_dosen.php?page=1">first</a></li>';
				    			$paging .= '<li><a href="kehadiran_dosen.php?page='.($page - 1).'">prev</a></li>';
				  			}	
							for($i=$start_page; $i<=$display_page; $i++){
								if($i == $page){
									$paging .= '<li><a href="#'.$i.'">'.$i.'</a></li>';
								}else{
									$paging .= '<li><a href="kehadiran_dosen.php?page='.$i.'">'.$i.'</a></li>';
								}
							}
							if($total_page > $display_page){
								$paging .= '<li><a href="kehadiran_dosen.php?page='.($page + 1).'">next</a></li>';
								$paging .= '<li><a href="kehadiran_dosen.php?page='.$total_page.'">last</a></li>';
							}
				   			$paging .= '<ul>';
				  }
				 echo $paging;
				 ?>


</body>
</html>
<?php mysqli_close($koneksi); ?>