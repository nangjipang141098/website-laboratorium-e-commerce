<center><h2>UBAH DATA CALON ASISTEN LABORATORIUM E-COMMERCE</h2></center>
<br>



<?php 
 include "koneksi.php";
 $ni = $_GET['ni'];
 $sql = "SELECT * FROM requitment WHERE npm='$ni'";
 $rs = mysqli_query($koneksi, $sql);
 $data = mysqli_fetch_assoc($rs);
 ?>


<!-- FORM -->
<form method="post" enctype="multipart/form-data" autocomplete="off">
	<div class="container">
		<div class="form-group row">
			<label class="col-sm-2 col-form-label"> NAMA </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="nama" placeholder="NAMA LENGKAP"  value="<?php echo $data['nama'] ; ?>" required>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label"> NPM </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="npm" placeholder="NPM" value="<?php echo $data['npm'] ; ?>" required>
			</div>
		</div>
		<div class="form-group row">
			<label for="qtty" class="col-sm-2 col-form-label">PRODI</label>
			<div class="col-sm-8">	
				<input type="radio" name="prodi" value="TI" <?php echo ($data['prodi'] == 'TI') ? "checked": "" ?> required> TEKNIK INFORMATIKA
                <input type="radio" name="prodi" value="SI" <?php echo ($data['prodi'] == 'SI') ? "checked": "" ?> > SISTEM INFORMASI </td>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> APAKAH ANDA BISA PROGRAMING? BAHASA PROGRAM APA YANG DIKUASAI? DESKRIPSIKAN SECARA SINGKAT </label>
			<div class="col-sm-8">
            <textarea class="form-control" rows="5" name="coding"><?php echo $data['programing'] ; ?></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> APAKAH ANDA BISA NETWORKING? SEJAUH MANA PENGETAHUAN ANDA TENTANG JARINGAN? DESKRIPSIKAN SECARA SINGKAT </label>
			<div class="col-sm-8">
            <textarea class="form-control" rows="5" name="jarkom"><?php echo $data['networking'] ; ?></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> APAKAH ANDA BISA DESIGN? SOFTWARE APA YANG BIASA DIGUNAKAN? DESKRIPSIKAN SECARA SINGKAT </label>
			<div class="col-sm-8">
            <textarea class="form-control" rows="5" name="design"><?php echo $data['design'] ; ?></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> ALASAN MENDAFTAR MENJADI ASISTEN LAB E-COMMERCE </label>
			<div class="col-sm-8">
            <textarea class="form-control" rows="5" name="alasan"><?php echo $data['alasan'] ; ?></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> DESKRIPSIKAN TENTANG ANDA </label>
			<div class="col-sm-8">
            <textarea class="form-control" rows="5" name="about"><?php echo $data['about'] ; ?></textarea>
			</div>
		</div>

        <div class="form-group row">
			<label  class="col-sm-2 col-form-label">CV</label>
			<div class="form-group col-sm-8">	
				<input type="file" name="foto" class="form-control">
			</div>
		</div>
   
     
        <div class="form-group row">
           <label  class="col-sm-2 col-form-label">HASIL</label>
			<div class="col-sm-8">
                <select name="hasil" class="form-control" required>
                    <option selected disabled>....</option>
					<option <?php echo ($data['lolos'] == 'LOLOS') ? "selected": "" ?>>LOLOS</option>
                    <option <?php echo ($data['lolos'] == 'LOLOS') ? "selected": "" ?>>TIDAK</option>
			    </select>
			</div>
		</div>
		<div class="form-group row">
		<div class="col-sm-10" style="float: right;">	
				<button class="btn btn-lg btn-danger" name="batal">BATAL</button>
				<button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
		</div>	
		</div>
	</div>
</form>

	<?php 
	include "koneksi.php";
	
	if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
	
	if (isset($_POST['tambah'])) 
	{
            
			$namafoto=$_FILES['foto'] ['name'];
			$lokasifoto =$_FILES['foto'] ['tmp_name'];
			$npm = $_POST['npm'];
			$nama = $_POST['nama'];
			$prodi = $_POST['prodi'];
            $about = $_POST['about'];
            $network = $_POST['jarkom'];
            $design = $_POST['design'];
            $coding = $_POST['coding'];
            $alasan = $_POST['alasan'];
            $keterangan = $_POST['hasil'];
			date_default_timezone_set('Asia/Jakarta');  
			$lu =  date("l, j F Y, H:i")  ;

            move_uploaded_file($lokasifoto, "../surat/rekruitment/$cv");

			if(empty($lokasifoto)){
            $kon = mysqli_query($koneksi, "UPDATE requitment SET 
            nama='$nama',
            prodi='$prodi',
            programing='$coding',
            design='$design',
            networking='$network',
            alasan='$alasan',
            about='$about',
            lolos='$keterangan' WHERE npm='$ni'
			");
			}
			else{
				$kon = mysqli_query($koneksi, "UPDATE requitment SET 
				nama='$nama',
				prodi='$prodi',
				programing='$coding',
				design='$design',
				networking='$network',
				alasan='$alasan',
				about='$about',
				cv='$namafoto',
				lolos='$keterangan' WHERE npm='$ni'
				");
			}

            
           

		echo "<script>alert('DATA DEKORASI TELAH DITAMBAHKAN');</script>";
		echo "<script>location='index.php?halaman=reqruitment';</script> ";
	
	}

	?>