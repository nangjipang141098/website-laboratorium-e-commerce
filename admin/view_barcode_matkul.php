<?php
//library phpqrcode
include "phpqrcode/qrlib.php";
include 'koneksi.php';
 
//direktory tempat menyimpan hasil generate qrcode jika folder belum dibuat maka secara otomatis akan membuat terlebih dahulu
$tempdir = "temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);
 
?>
<html>
<head>
</head>
<body>
   
<a href="index.php?halaman=daftar_matkul" class="btn btn-primary btn-lg btn-block">Kembali</a><br>
 
        <?php
            $no = 1;
            $ni = $_GET['ni'];
            $query = "SELECT * FROM matkul where kode_matkul='$ni' AND tahun_ajaran='2019-2020'";
            $arsip1 = $koneksi->prepare($query);
            $arsip1->execute();
            $res1 = $arsip1->get_result();
            while ($row = $res1->fetch_assoc()) {
                $kode_mk = $row['kode_matkul'];
                $dosen = $row['dosen'];
                $matkul = $row['matkul'];
                $kelas = $row['kelas'];
                $hari = $row['hari'];
                $jam = $row['jam'];
                $tahun = $row['tahun_ajaran'];
                $w =  $kode_mk . " " . $matkul . "  " . $kelas . " " . $tahun . " " . $dosen ;
                //Isi dari QRCode Saat discan
                $isi_teks1 = $w;
                //Nama file yang akan disimpan pada folder temp 
                $namafile1 = $w.".png";
                //Kualitas dari QRCode 
                $quality1 = 'H'; 
                //Ukuran besar QRCode
                $ukuran1 = 4; 
                $padding1 = 0; 
                QRCode::png($isi_teks1,$tempdir.$namafile1,$quality1,$ukuran1,$padding1);
        ?>
                QR CODE  
                <pre><img src="temp/<?php echo $namafile1; ?>" width="400px"></pre><br>
                KODE MATKUL 
                <pre><?php echo $row['kode_matkul'] ; ?></pre><br>
                MATKUL
                <pre><?php echo $row['matkul'] ; ?></pre><br>
                HARI 
                <pre><?php echo $row['hari'] ; ?></pre><br>
                JAM 
                <pre><?php echo $row['jam'] ; ?></pre><br>
                DOSEN 
                <pre><?php echo $row['dosen'] ; ?></pre><br>
                KELAS 
                <pre><?php echo $row['kelas'] ; ?></pre><br>
                    
                
              
        <?php } ?>
       
</body>
</html>
<?php mysqli_close($koneksi); ?>