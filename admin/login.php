﻿
 <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>SELAMAT DATANG | E-COMMERCE  </title>
  <style> 
    #nav1
    {  
      background-color:#F00;  /*whichever you want*/
      opacity: 0.9;           /*0.5 determines transparency value*/ 
      filter:(opacity=50);
      background: transparent;
      background-image: none;       
    }
    .carousel-item img {
      filter: grayscale(0);
      opacity: 0.8 !important;
      height: 600px;
      transition: 300;
    }
    h1, p
    {
      text-shadow: 1px 1px 3px rgba(0,0,0,0.5);ss
    }
  </style>
  <link rel="shortcut icon" href="assets/img/logo.jpg">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  
       <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
       
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

</head>
<body>
  <!-- Navbar -->
  <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark" id="nav1">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="nav navbar-brand">
      <button class="btn btn-outline-primary my-2 my-sm-0"><li><a href="form_login.php"><span class="fa fa-key">Login</span></a></li></button>  
      <button class="btn btn-outline-primary my-2 my-sm-0"><li><a href="view_rekruitment.php"><span class="fa fa-key">Rekruitment</span></a></li></button>  
      <button class="btn btn-outline-primary my-2 my-sm-0"><li><a href="view_struktur.php"><span class="fa fa-key">Struktur Organisasi</span></a></li></button>  
    
      </ul>
            </div>
  </nav>

  <!-- Carousel -->
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img class="d-block w-100" src="assets\img\unasdanlab.jpg" alt="First slide">
                  <div class="carousel-caption d-none d-md-block">
                    <h1>
                        Selamat Datang
                    </h1>
                    <p>WEBSITE LABORATORIUM E-COMMERCE</p>
                  </div>
                </div>
                <div class="carousel-item">
                  <img class="d-block w-100" src="assets\img\TEST.jpg" alt="Second slide">
                  <div class="carousel-caption d-none d-md-block">
                    <h1>
                        Selamat Datang
                    </h1>
                    <p>WEBSITE LABORATORIUM E-COMMERCE</p>
                  </div>
                </div>
                <div class="carousel-item">
                  <img class="d-block w-100" src="assets\img\hmmm.jpg" alt="Third slide">
                  <div class="carousel-caption d-none d-md-block">
                    <h1>
                       Selamat Datang
                    </h1>
                    <p>WEBSITE LABORATORIUM E-COMMERCE</p>
                  </div>
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>




<!-- 
  <script>
     const slider = document.querySelectorAll('.slider');
        M.Slider.init(slider,{
          indicators: false,
          height: 500,
          transition: 600,
          interval: 3000,
        });
  </script> -->


  <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</style>
</body>
</html>