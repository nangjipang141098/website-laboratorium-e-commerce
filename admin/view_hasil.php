<?php 
// session_start();
// if(empty($_SESSION['status_login'])){
// 	header('location:login.php');
// }

	include "koneksi.php"; 
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Data Logistik Pandamanda</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="shortcut icon" href="assets/img/favicon.ico">
   	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<!-- TOP NAV -->
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Pandamanda</a> 
            </div>
            <div style="color: white; padding: 15px 20px 5px 20px; float: left; font-size: 20px; width: 670px;">
			<?php  date_default_timezone_set('Asia/Jakarta'); echo  date("l, j F Y, H:i");  ?> &nbsp; &nbsp; &nbsp; &nbsp;
            </div>
		  
	    </nav>   
   		<!-- SIDE NAV -->
	    <nav class="navbar-default navbar-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav" id="main-menu">				
					<li><a href="view_rekruitment.php"><i class="fa fa-home fa-3x"></i> Isi Formulir </a></li>
					<li><a href="view_hasil.php"><i class="fa fa-diamond fa-3x"></i>Hasil Tes</a></li>
					<li><a href="view_pengumuman.php"><i class="fa fa-diamond fa-3x"></i>Kriteria</a></li>
					<li><a href="login.php"><i class="fa fa-user fa-3x"></i> Back</a></li>		
				</ul>
			</div>     
	    </nav>
	    <!-- PAGE INNER  -->
	    <div id="page-wrapper" >
	        <div id="page-inner">    

<!-- FORM -->
<form method="post" enctype="multipart/form-data" autocomplete="off">
<h2> BERIKUT ADALAH HASIL SELEKSI CALON ASISTEN LABORATORIUM </h2>
<div class="btn-group" role="group" aria-label="Basic example">
               
                </div>
				
				<div class="table-responsive">	
					<table class='table table-bordered'>
						<thead>
							<tr>
							<th>Nama</th>
							<th>NPM</th>
							<th>PRODI</th>	
							<th>FOTO</th>	
                            <th>HASIL</th>				
							</tr>
						</thead>
						<tbody>
							<?php
								if (isset($_POST['reload'])) 
								{
									echo "<script>location='index.php?halaman=akses';</script> ";
								}
							?>
							<?php 
							if (isset($_POST['ubah'])) 
							{
								$kategori = $_POST['kategori'];
								$input = $_POST['input'];
								$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
						 		$perpage = 7;
								$limit = ($page - 1) * $perpage;
								$prev = 1;
								$next = 2;
								$start_page = ($page - $prev) < 1 ? 1 : ($page - $prev);
								$tahun = getdate();
								$n = $tahun['year'];
								$sql = "SELECT * FROM requitment where tahun ='$n'";
								$rs = mysqli_query($koneksi, $sql);
								$record = mysqli_num_rows($rs);
								$total_page = ceil($record / $perpage);
								$display_page = $start_page + $prev + $next;
								if($display_page > $total_page){
								$display_page = $total_page;
								}
								$sql .= ' LIMIT '.$limit.','.$perpage;
								$rs = mysqli_query($koneksi, $sql);
								switch($kategori){
									case 1:	
										$rs = mysqli_query($koneksi, "SELECT * FROM requitment WHERE tahun_masuk='$input'");
									break;
									case 2:		
										$rs = mysql_query($koneksi, "SELECT * FROM arequitment WHERE npm='$input'");
									break;
									case 3:			
										$rs = mysql_query($koneksi, "SELECT * FROM requitment WHERE prodi='$input'");
									break;
								}
										
							}
							
							else{
						 		$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
						 		$perpage = 7;
								$limit = ($page - 1) * $perpage;
								$prev = 1;
								$next = 2;
								$start_page = ($page - $prev) < 1 ? 1 : ($page - $prev);
								$tahun = getdate();
								$n = $tahun['year'];
								$sql = "SELECT * FROM requitment where tahun ='$n'";
								$rs = mysqli_query($koneksi, $sql);
								$record = mysqli_num_rows($rs);
								$total_page = ceil($record / $perpage);
								$display_page = $start_page + $prev + $next;
								if($display_page > $total_page){
								$display_page = $total_page;
								}
								$sql .= ' LIMIT '.$limit.','.$perpage;
								$rs = mysqli_query($koneksi, $sql);
								}
							 ?>
							
							<?php while($data = mysqli_fetch_assoc($rs)){ ?>
								<tr>
								<td width="60"><?php echo $data['nama'] ; ?></td>
								<td width="70"><?php echo $data['npm'] ; ?></td>
								<td width="30"><?php echo $data['prodi'] ; ?></td>
                                <td width="60"><img src="../foto_dekorasi/<?php echo $data['foto'] ;?>" width="100"></td>
								<td width="40"><?php echo $data['lolos'] ; ?></td>
								</tr>
							<?php } ?>
							
						</tbody>
					</table>
				</div>
				<?php
				  	$paging = null;
					if($total_page > 1){
					   	$paging .= '<ul class="pagination">';
				  			if($page > ($prev + 1)){
				   				$paging .= '<li><a href="view_hasil.php&page=1">first</a></li>';
				    			$paging .= '<li><a href="view_hasil.php&page='.($page - 1).'">prev</a></li>';
				  			}	
							for($i=$start_page; $i<=$display_page; $i++){
								if($i == $page){
									$paging .= '<li><a href="#'.$i.'">'.$i.'</a></li>';
								}else{
									$paging .= '<li><a href="view_hasil.php&page='.$i.'">'.$i.'</a></li>';
								}
							}
							if($total_page > $display_page){
								$paging .= '<li><a href="view_hasil.php&page='.($page + 1).'">next</a></li>';
								$paging .= '<li><a href="view_hasil.php&page='.$total_page.'">last</a></li>';
							}
				   			$paging .= '<ul>';
				  }
				 echo $paging;
				 ?>
            </div>
        
	</body>
</html>

	<?php 
	include "koneksi.php";
	
	
	if (isset($_POST['tambah'])) 
	{
            $ktm=$_FILES['ktm'] ['name'];
            $foto=$_FILES['foto'] ['name'];
            $transkip=$_FILES['transkip'] ['name'];
            $cv=$_FILES['cv'] ['name'];
			$lokasifoto =$_FILES['foto'] ['tmp_name'];
			$npm = $_POST['npm'];
			$nama = $_POST['nama'];
			$prodi = $_POST['prodi'];
            $about = $_POST['about'];
            $network = $_POST['jarkom'];
            $design = $_POST['design'];
            $coding = $_POST['coding'];
            $alasan = $_POST['alasan'];
            $keterangan = "LOLOS";
			date_default_timezone_set('Asia/Jakarta');  
			$lu =  date("l, j F Y, H:i")  ;

            move_uploaded_file($lokasifoto, "../foto_dekorasi/$ktm");
            move_uploaded_file($lokasifoto, "../foto_dekorasi/$foto");
			move_uploaded_file($lokasifoto, "../foto_display/$transkip");
            move_uploaded_file($lokasifoto, "../foto_display/$cv");
         
            $kon = mysqli_query($koneksi, "INSERT INTO requitment
            (npm, nama, prodi, programing, design, networking, cv, foto, ktm, transkip, alasan, about, lolos) 
            VALUES 
            ('$npm','$nama','$prodi','$coding','$design','$network','$cv','$foto','$ktm','$transkip','$alasan','$about','$keterangan')
			");
            
           

		echo "<script>alert('ANDA TELAH MENDAFTAE SEBAGAI CALON ASLAB E-COMMERCE');</script>";
		echo "<script>location='login.php';</script> ";
	
	}

	?>


    
</div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>	
	</body>
</html>
