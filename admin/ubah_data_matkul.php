

<?php 
 include "koneksi.php";
	
 if(!isset($_SESSION)) 
 { 
     session_start(); 
 } 
 $ni = $_GET['ni'];
 $sql = "SELECT * FROM matkul WHERE kode_matkul='$ni' AND tahun_ajaran='2019-2020'";
 $rs = mysqli_query($koneksi, $sql);
 $data = mysqli_fetch_assoc($rs);
 ?>

<center><h2>EDIT DATA MATA KULIAH</h2></center>
<br>

<!-- FORM -->
<form method="post" enctype="multipart/form-data" autocomplete="off">
	<div class="container">
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> MATA KULIAH </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="matkul" value="<?php echo $data['matkul'] ; ?>"  required>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> HARI </label>
			<div class="col-sm-8">
                <input type="radio" name="hari" value="SENIN" <?php echo ($data['hari'] == 'SENIN') ? "checked": "" ?> > SENIN
                <input type="radio" name="hari" value="SELASA" <?php echo ($data['hari'] == 'SELASA') ? "checked": "" ?> > SELASA 
                <input type="radio" name="hari" value="RABU" <?php echo ($data['hari'] == 'RABU') ? "checked": "" ?> > RABU <br>
                <input type="radio" name="hari" value="KAMIS" <?php echo ($data['hari'] == 'KAMIS') ? "checked": "" ?> > KAMIS 
                <input type="radio" name="hari" value="JUMAT" <?php echo ($data['hari'] == 'JUMAT') ? "checked": "" ?> > JUMAT
                <input type="radio" name="hari" value="SABTU" <?php echo ($data['hari'] == 'SABTU') ? "checked": "" ?> > SABTU 
			</div>
		</div>
        <div class="form-group row">
                <label class="col-sm-2 col-form-label"> JAM </label>
                <div class="col-sm-8">	
                <select name="jam" class="form-control" required>
                        <option selected="selected"></option>
                        <option <?php echo ($data['jam'] == '08.00-09.40') ? "selected": "" ?>> 08.00-09.40 </option>
                        <option <?php echo ($data['jam'] == '09.50-11.30') ? "selected": "" ?>> 09.50-11.30 </option>
                        <option <?php echo ($data['jam'] == '11.40-13.20') ? "selected": "" ?>> 11.40-13.20 </option>
                        <option <?php echo ($data['jam'] == '13.30-15.10') ? "selected": "" ?>> 13.30-15.10 </option>
                        <option <?php echo ($data['jam'] == '15.20-17.00') ? "selected": "" ?>> 15.20-17.00 </option>
                        <option <?php echo ($data['jam'] == '17.00-18.40') ? "selected": "" ?>> 17.00-18.40 </option>
                        <option <?php echo ($data['jam'] == '18.50-20.20') ? "selected": "" ?>> 18.50-20.20 </option>
                        <option <?php echo ($data['jam'] == '20.20-22.00') ? "selected": "" ?>> 20.20-22.00 </option>
			    </select>
                </div>
        </div>	
     
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> DOSEN </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="dosen" value="<?php echo $data['dosen'] ; ?>"  required>
			</div>
		</div>
		<div class="form-group row">
                <label class="col-sm-2 col-form-label"> TAHUN AJARAN </label>
                <div class="col-sm-8">	
                <select name="tahun_ajaran" class="form-control" required>
                        <option selected="selected"></option>
                        <option <?php echo ($data['tahun_ajaran'] == '2019-2020') ? "selected": "" ?>> 2019-2020 </option>
                        <option <?php echo ($data['tahun_ajaran'] == '2020-2021') ? "selected": "" ?>> 2020-2021 </option>
                        <option <?php echo ($data['tahun_ajaran'] == '2021-2022') ? "selected": "" ?>> 2021-2022 </option>
                        <option <?php echo ($data['tahun_ajaran'] == '2022-2023') ? "selected": "" ?>> 2022-2023 </option>
                        <option <?php echo ($data['tahun_ajaran'] == '2023-2024') ? "selected": "" ?>> 2023-2024 </option>
                        <option <?php echo ($data['tahun_ajaran'] == '2024-2025') ? "selected": "" ?>> 2024-2025 </option>
			    </select>
			    </select>
                </div>
        </div>	
        
		<div class="form-group row">
		<div class="col-sm-10" style="float: right;">	
				<button class="btn btn-lg btn-danger" name="batal">BATAL</button>
				<button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
		</div>	
		</div>
	</div>
</form>

	<?php 
	
	
	if (isset($_POST['tambah'])) 
	{
			
			$matkul = $_POST['matkul'];
			$hari = $_POST['hari'];
			$jam = $_POST['jam'];
			$dosen = $_POST['dosen'];
			$tahun_ajaran = $_POST['tahun_ajaran'];
			date_default_timezone_set('Asia/Jakarta');  
			$lu =  date("l, j F Y, H:i")  ;
		
			
            $kon = mysqli_query($koneksi, "UPDATE matkul SET 
                                matkul       = '$matkul',
                                hari         = '$hari',
                                jam          = '$jam',
                                dosen        = '$dosen',
                                tahun_ajaran = '$tahun_ajaran'
                                WHERE kode_matkul  = '$ni'
					");
            
           

		echo "<script>alert('DATA SURAT KELUAR TELAH DITAMBAHKAN');</script>";
		echo "<script>location='index.php?halaman=daftar_matkul';</script> ";
	
	}

	?>