<?php
//library phpqrcode
include "phpqrcode/qrlib.php";
include 'koneksi.php';
 
//direktory tempat menyimpan hasil generate qrcode jika folder belum dibuat maka secara otomatis akan membuat terlebih dahulu
$tempdir = "temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);
 
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>QR FODE | LAB E-COMMERCE</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="shortcut icon" href="assets/img/logo.jpg">
    </head>
    <form>
            
         
 
        <?php
            $no = 1;
            $ni = $_GET['ni'];
            $query = "SELECT * FROM matkul where kode_matkul='$ni' AND tahun_ajaran='2019-2020'";
            $arsip1 = $koneksi->prepare($query);
            $arsip1->execute();
            $res1 = $arsip1->get_result();
            while ($row = $res1->fetch_assoc()) {
                $kode_mk = $row['kode_matkul'];
                $dosen = $row['dosen'];
                $matkul = $row['matkul'];
               
                $hari = $row['hari'];
                $jam = $row['jam'];
                $tahun = $row['tahun_ajaran'];
                $w = $dosen . "  " . $kode_mk . " " . $matkul . "  " . $tahun;
                //Isi dari QRCode Saat discan
                $isi_teks1 = $w;
                //Nama file yang akan disimpan pada folder temp 
                $namafile1 = $w.".png";
                //Kualitas dari QRCode 
                $quality1 = 'H'; 
                //Ukuran besar QRCode
                $ukuran1 = 4; 
                $padding1 = 0; 
                QRCode::png($isi_teks1,$tempdir.$namafile1,$quality1,$ukuran1,$padding1);
        ?>      
               
             
               <body>
  <div class="container">	
    <div class="row text-center ">
      <div class="col-md-12">
        <br><br>
        <center></center>
        <br>
      </div>
    </div>
    <div class="row ">
      <div class="col-md-5 col-md-offset-4 col-sm-5 col-sm-offset-3 col-xs-10 col-xs-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>Quick Response Code </strong>  
          </div>
          <div class="panel-body">
            <form role="form" method="post" autocomplete="off">
              <div class="form-group input-group">
                <img src="temp/<?php echo $namafile1; ?>" width="423px"><br>
              </div>
              <div class="form-group input-group">
                <br>
                <a href="index.php?halaman=daftar_matkul" class="btn btn-info">Kembali</a>
              </div>
           </div>
          </div>
        </div>
    </div>
              
        <?php } ?>
       
</body>
</form>
</html>
<?php mysqli_close($koneksi); ?>