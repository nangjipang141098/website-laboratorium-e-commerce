<?php
//library phpqrcode
include "phpqrcode/qrlib.php";
include 'koneksi.php';
 
//direktory tempat menyimpan hasil generate qrcode jika folder belum dibuat maka secara otomatis akan membuat terlebih dahulu
$tempdir = "temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);
 
?>
<html>
<head>
</head>
<body>

  <center><h4><b>JADWAL MATA KULIAH LABORATORIUM E-COMMERCE</b></h4></center>
 
    <table border="1" align="left">
        <thead>
            <th><center>HARI<center></th>
            <th><center>JAM<center></th>
            <th><center>MATKUL<center></th>
            <th><center>DOSEN<center></th>
        
          
        </thead>
        <tbody>
        <?php
        
           $sql = "SELECT * FROM matkul WHERE tahun_ajaran='2019-2020' AND hari IN ('JUMAT', 'SABTU')";
  
           $rs = mysqli_query($koneksi, $sql);
           

            while ($row = mysqli_fetch_assoc($rs)) {
                $kode_mk = $row['kode_matkul'];
                $dosen = $row['dosen'];
                $matkul = $row['matkul'];
                $kelas = $row['kelas'];
                $hari = $row['hari'];
                $jam = $row['jam'];
                $tahun = $row['tahun_ajaran'];
                $w = $dosen . "  " . $kode_mk . " " . $matkul . "  " . $kelas . " " . $hari . " " . $jam . " " . $tahun;
                //Isi dari QRCode Saat discan
                $isi_teks1 = $w;
                //Nama file yang akan disimpan pada folder temp 
                $namafile1 = $w.".png";
                //Kualitas dari QRCode 
                $quality1 = 'H'; 
                //Ukuran besar QRCode
                $ukuran1 = 4; 
                $padding1 = 0; 
                QRCode::png($isi_teks1,$tempdir.$namafile1,$quality1,$ukuran1,$padding1);
        ?>
            <tr>
                <td><?php echo $row['hari']; ?></td>
                <td><?php echo $row['jam']; ?></td>
                <td><?php echo $row['matkul']; ?> </td>
                <td><?php echo $row['dosen']; ?></td>              
            </tr>
        <?php } ?>
        </tbody>
    </table>
  
    <script>
        window.print();
    </script>
           
           


</body>
</html>
<?php mysqli_close($koneksi); ?>