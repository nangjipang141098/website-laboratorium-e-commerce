<?php
//library phpqrcode
include "phpqrcode/qrlib.php";
include 'koneksi.php';
 
//direktory tempat menyimpan hasil generate qrcode jika folder belum dibuat maka secara otomatis akan membuat terlebih dahulu
$tempdir = "temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);
 
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Berita Acara Perkuliahan</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="shortcut icon" href="assets/img/logo.jpg">
   	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<!-- TOP NAV -->
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">E-commerce</a> 
            </div>
            <div style="color: white; padding: 15px 20px 5px 20px; float: left; font-size: 20px; width: 670px;">
			<?php  date_default_timezone_set('Asia/Jakarta'); echo  date("l, j F Y, H:i");  ?> <a href="test_barcode.php" class="btn btn-success"> Scan Code QR</a> &nbsp; &nbsp; &nbsp; &nbsp;
            </div>
		  
	    </nav>   
   		<!-- SIDE NAV -->
	    <nav class="navbar-default navbar-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav" id="main-menu">				
					<li><a href="view_matkul.php"><i class="fa fa-home fa-3x"></i> Mata Kuliah </a></li>
					<li><a href="login.php"><i class="fa fa-diamond fa-3x"></i>Close</a></li>	
				</ul>
			</div>     
	    </nav>
	    <!-- PAGE INNER  -->
	    <div id="page-wrapper" >
	        <div id="page-inner">    
           
    <table class="table table-striped table-dark" border="3" align="center">
        <thead>
            <th><center>KODE MK<center></th>
            <th><center>HARI<center></th>
            <th><center>JAM<center></th>
            <th><center>MATKUL<center></th>
            <th><center>KELAS<center></th>
            <th><center>DOSEN</center></th>
            <th><center>QR CODE<center></th>
            <th><center>TINDAKAN<center></th>
        </thead>
        <tbody>
        <?php
            $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
            $perpage = 8;
           $limit = ($page - 1) * $perpage;
           $prev = 1;
           $next = 2;
           $start_page = ($page - $prev) < 1 ? 1 : ($page - $prev);
           $sql = "SELECT * FROM matkul WHERE tahun_ajaran='2019-2020'";
           $rs = mysqli_query($koneksi, $sql);
           $record = mysqli_num_rows($rs);
           $total_page = ceil($record / $perpage);
           $display_page = $start_page + $prev + $next;
           if($display_page > $total_page){
           $display_page = $total_page;
           }
           $sql .= ' LIMIT '.$limit.','.$perpage;
           $rs = mysqli_query($koneksi, $sql);
           

            while ($row = mysqli_fetch_assoc($rs)) {
                $kode_mk = $row['kode_matkul'];
                $dosen = $row['dosen'];
                $matkul = $row['matkul'];
                $kelas = $row['kelas'];
                $hari = $row['hari'];
                $jam = $row['jam'];
                $tahun = $row['tahun_ajaran'];
                $w = $dosen . "  " . $kode_mk . " " . $matkul . "  " . $kelas . " " . $hari . " " . $jam . " " . $tahun;
                //Isi dari QRCode Saat discan
                $isi_teks1 = $w;
                //Nama file yang akan disimpan pada folder temp 
                $namafile1 = $w.".png";
                //Kualitas dari QRCode 
                $quality1 = 'H'; 
                //Ukuran besar QRCode
                $ukuran1 = 4; 
                $padding1 = 0; 
                QRCode::png($isi_teks1,$tempdir.$namafile1,$quality1,$ukuran1,$padding1);
        ?>
            <tr>
                <td><?php echo $row['kode_matkul']; ?></td>
                <td><?php echo $row['hari']; ?></td>
                <td><?php echo $row['jam']; ?></td>
                <td><?php echo $row['matkul']; ?></td>
                <td><?php echo $row['kelas']; ?></td>
                <td><?php echo $row['dosen']; ?></td>       
                <td style="padding: 10px;"><img src="temp/<?php echo $namafile1; ?>" width="35px"></td>
                <td>
					<a href="view_barcode_matkul_dosen.php?ni=<?php echo $row['kode_matkul'] ; ?>" class="btn btn-info">View Code QR</a>
				</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

             <?php
				  	$paging = null;
					if($total_page > 1){
					   	$paging .= '<ul class="pagination">';
				  			if($page > ($prev + 1)){
				   				$paging .= '<li><a href="view_matkul.php?page=1">first</a></li>';
				    			$paging .= '<li><a href="view_matkul.php?page='.($page - 1).'">prev</a></li>';
				  			}	
							for($i=$start_page; $i<=$display_page; $i++){
								if($i == $page){
									$paging .= '<li><a href="#'.$i.'">'.$i.'</a></li>';
								}else{
									$paging .= '<li><a href="view_matkul.php?page='.$i.'">'.$i.'</a></li>';
								}
							}
							if($total_page > $display_page){
								$paging .= '<li><a href="view_matkul.php?page='.($page + 1).'">next</a></li>';
								$paging .= '<li><a href="view_matkul.php?page='.$total_page.'">last</a></li>';
							}
				   			$paging .= '</ul>';
				  }
				 echo $paging;
				 ?>


</body>
</html>
<?php mysqli_close($koneksi); ?>

</div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>	
	</body>
</html>
