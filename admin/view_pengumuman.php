
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Data Logistik Pandamanda</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="shortcut icon" href="assets/img/favicon.ico">
   	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<!-- TOP NAV -->
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Pandamanda</a> 
            </div>
            <div style="color: white; padding: 15px 20px 5px 20px; float: left; font-size: 20px; width: 670px;">
			<?php  date_default_timezone_set('Asia/Jakarta'); echo  date("l, j F Y, H:i");  ?> &nbsp; &nbsp; &nbsp; &nbsp;
            </div>
		  
	    </nav>   
   		<!-- SIDE NAV -->
	    <nav class="navbar-default navbar-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav" id="main-menu">				
					<li><a href="view_rekruitment.php"><i class="fa fa-home fa-3x"></i> Isi Formulir </a></li>
					<li><a href="view_hasil.php"><i class="fa fa-diamond fa-3x"></i>Hasil Tes</a></li>
					<li><a href="view_pengumuman.php"><i class="fa fa-diamond fa-3x"></i>Kriteria</a></li>
					<li><a href="login.php"><i class="fa fa-user fa-3x"></i> Back</a></li>		
				</ul>
			</div>     
	    </nav>
	    <!-- PAGE INNER  -->
	    <div id="page-wrapper" >
	        <div id="page-inner">    



<?php 
    include "koneksi.php"; 
?>

<?php
$rs = mysqli_query($koneksi, "SELECT * FROM pengumuman");
while($data = mysqli_fetch_assoc($rs)){ ?>
		<tr>
			<td width="60"><?php echo $data['pengumuman'] ; ?></td><br>
			<td width="70"><?php echo $data['kriteria'] ; ?></td>
		</tr>
<?php } ?>
<br>
<br>

</div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>	
	</body>
</html>