<?php include "koneksi.php"; ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
				<!-- TABLE DEKORASI -->
				<div class="table-responsive">	
					<table class='table table-bordered'>
						<tr>
							<td>
								<form method="post" enctype="multipart/form-data" autocomplete="off">
									<div class="form-group">
										<label> CARI BERDASARKAN </label>
										<select name="kategori">
											<option selected disabled>....</option>
											<option value="1"> NOMER LAMPIRAN </option>
											<option value="2"> PERIHAL </option>
										</select>
									<input type="text" class="form-control" name="input" size="70">
									</div>
										<button class="btn btn-info muted" name="ubah">CARI</button>
										<button class="btn btn-muted" name="reload">RELOAD</button>
								</form>
							</td>	
						<tr>
					</table>
				</div>
				<a href="index.php?halaman=tambah_data_surat_masuk" class="btn btn-primary btn-lg btn-block">Tambah Data Surat Masuk</a>
				<div class="table-responsive">	
					<table class='table table-bordered'>
						<thead>
							<tr>
							<th>No.Lampiran</th>
							<th>Perihal</th>
							<th>Dari</th>
							<th>Dokumen</th>
							<th>Aksi</th>					
							</tr>
						</thead>
						<tbody>
							<?php
								if (isset($_POST['reload'])) 
								{
									echo "<script>location='index.php?halaman=surat_masuk';</script> ";
								}
							?>
							<?php 
							if (isset($_POST['ubah'])) 
							{
								$kategori = $_POST['kategori'];
								$input = $_POST['input'];
								$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
						 		$perpage = 7;
								$limit = ($page - 1) * $perpage;
								$prev = 1;
								$next = 2;
								$start_page = ($page - $prev) < 1 ? 1 : ($page - $prev);
								$sql = 'SELECT * FROM surat_masuk';
								$rs = mysqli_query($koneksi, $sql);
								$record = mysqli_num_rows($rs);
								$total_page = ceil($record / $perpage);
								$display_page = $start_page + $prev + $next;
								if($display_page > $total_page){
								$display_page = $total_page;
								}
								$sql .= ' LIMIT '.$limit.','.$perpage;
								$rs = mysqli_query($koneksi, $sql);
								switch($kategori){
									case 1:	
										$rs = mysqli_query($koneksi, "SELECT * FROM surat_masuk WHERE nomer='$input'");
									break;
									case 2:		
										$rs = mysqli_query($koneksi, "SELECT * FROM surat_masuk WHERE perihal='$input'");
									break;
								}
										
							}
							
							else{
						 		$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
						 		$perpage = 7;
								$limit = ($page - 1) * $perpage;
								$prev = 1;
								$next = 2;
								$start_page = ($page - $prev) < 1 ? 1 : ($page - $prev);
								$sql = 'SELECT * FROM surat_masuk';
								$rs = mysqli_query($koneksi, $sql);
								$record = mysqli_num_rows($rs);
								$total_page = ceil($record / $perpage);
								$display_page = $start_page + $prev + $next;
								if($display_page > $total_page){
								$display_page = $total_page;
								}
								$sql .= ' LIMIT '.$limit.','.$perpage;
								$rs = mysqli_query($koneksi, $sql);
								}
							 ?>
							
							<?php while($data = mysqli_fetch_assoc($rs)){ ?>
								<tr>
								<td width="50"><?php echo $data['nomer'] ; ?></td>
								<td width="50"><?php echo $data['perihal'] ; ?></td>
								<td width="50"><?php echo $data['dari'] ; ?></td>
								<td width="60"><?php echo $data['dokumen'] ;?></td>
								<td  width="50">
									<a href="index.php?halaman=hapus_data_surat_masuk&ni=<?php echo $data['nomer'] ; ?> " class="btn btn-danger" onClick="return confirm('Apakah anda yakin data ini akan di hapus secara permanen?');">Hapus</a>
									<a href="index.php?halaman=ubah_data_surat_masuk&ni=<?php echo $data['nomer'] ; ?>" class="btn btn-warning">Edit</a>
									<a href="index.php?halaman=view_data_surat_masuk&ni=<?php echo $data['nomer'] ; ?>" class="btn btn-info">View</a>
								</td>
								</tr>
							<?php } ?>
							
						</tbody>
					</table>
				</div>
				<?php
				  	$paging = null;
					if($total_page > 1){
					   	$paging .= '<ul class="pagination">';
				  			if($page > ($prev + 1)){
				   				$paging .= '<li><a href="index.php?halaman=surat_masuk&page=1">first</a></li>';
				    			$paging .= '<li><a href="index.php?halaman=surat_masuk&page='.($page - 1).'">prev</a></li>';
				  			}	
							for($i=$start_page; $i<=$display_page; $i++){
								if($i == $page){
									$paging .= '<li><a href="#'.$i.'">'.$i.'</a></li>';
								}else{
									$paging .= '<li><a href="index.php?halaman=surat_masuk&page='.$i.'">'.$i.'</a></li>';
								}
							}
							if($total_page > $display_page){
								$paging .= '<li><a href="index.php?halaman=surat_masuk&page='.($page + 1).'">next</a></li>';
								$paging .= '<li><a href="index.php?halaman=surat_masuk&page='.$total_page.'">last</a></li>';
							}
				   			$paging .= '<ul>';
				  }
				 echo $paging;
				 ?>
            </div>
        
	</body>
</html>
