<center><h2>TAMBAH STRUKTUR ORGANISASI </h2></center>
<br>
<!-- FORM -->



   
<form method="post" enctype="multipart/form-data" autocomplete="off">

<div class="container">	
<h5><b>MASUKAN TAHUN PRIODE JABATAN </b></h5>
        <div class="form-group row">
                <div class="col-sm-8">	
                <select name="tahun" class="form-control" required>
                    <option selected disabled>....</option>
					<option value="2019"> 2019 </option>
					<option value="2020"> 2020 </option>
					<option value="2021"> 2021 </option>
					<option value="2022"> 2022 </option>
					<option value="2023"> 2023 </option>
					<option value="2024"> 2024 </option>
			    </select>
                </div>
        </div>	


        <h5><b>MASUKAN NAMA DAN JABATAN </b></h5>
        <div class="form-group row">		
			<div class="col-sm-8">
				<input type="text" class="form-control" name="kalab" placeholder="NAMA LENGKAP"  required>
                <select name="jkalab" class="form-control" required>
                    <option selected disabled>....</option>
					<option value="KEPALA LABORATORIUM"> KEPALA LABORATORIUM </option>
					<option value="KORDINATOR LABORATORIUM"> KORDINATOR LABORATORIUM </option>
					<option value="SEKRETARIS LABORATORIUM"> SEKRETARIS LABORATORIUM </option>
					<option value="PENJADWALAN LABORATORIUM"> PENJADWALAN LABORATORIUM </option>
					<option value="HUMAS I LABORATORIUM"> HUMAS I LABORATORIUM </option>
					<option value="HUMAS II LABORATORIUM"> HUMAS II LABORATORIUM </option>
					<option value="PENGEMBANGAN I LABORATORIUM"> PENGEMBANGAN I LABORATORIUM </option>
					<option value="PENGEMBANGAN II LABORATORIUM"> PENGEMBANGAN II LABORATORIUM </option>
			    </select>
			</div>
            
		</div>
        <div class="form-group row">
			<div class="col-sm-8">
				<input type="text" class="form-control" name="koor" placeholder="NAMA LENGKAP"  required>
                <select name="jkoor" class="form-control" required>
                    <option selected disabled>....</option>
					<option value="KEPALA LABORATORIUM"> KEPALA LABORATORIUM </option>
					<option value="KORDINATOR LABORATORIUM"> KORDINATOR LABORATORIUM </option>
					<option value="SEKRETARIS LABORATORIUM"> SEKRETARIS LABORATORIUM </option>
					<option value="PENJADWALAN LABORATORIUM"> PENJADWALAN LABORATORIUM </option>
					<option value="HUMAS I LABORATORIUM"> HUMAS I LABORATORIUM </option>
					<option value="HUMAS II LABORATORIUM"> HUMAS II LABORATORIUM </option>
					<option value="PENGEMBANGAN I LABORATORIUM"> PENGEMBANGAN I LABORATORIUM </option>
					<option value="PENGEMBANGAN II LABORATORIUM"> PENGEMBANGAN II LABORATORIUM </option>
			    </select>
			</div>
		</div>
        <div class="form-group row">
			<div class="col-sm-8">
				<input type="text" class="form-control" name="seklab" placeholder="NAMA LENGKAP"  required>
                <select name="jseklab" class="form-control" required>
                    <option selected disabled>....</option>
					<option value="KEPALA LABORATORIUM"> KEPALA LABORATORIUM </option>
					<option value="KORDINATOR LABORATORIUM"> KORDINATOR LABORATORIUM </option>
					<option value="SEKRETARIS LABORATORIUM"> SEKRETARIS LABORATORIUM </option>
					<option value="PENJADWALAN LABORATORIUM"> PENJADWALAN LABORATORIUM </option>
					<option value="HUMAS I LABORATORIUM"> HUMAS I LABORATORIUM </option>
					<option value="HUMAS II LABORATORIUM"> HUMAS II LABORATORIUM </option>
					<option value="PENGEMBANGAN I LABORATORIUM"> PENGEMBANGAN I LABORATORIUM </option>
					<option value="PENGEMBANGAN II LABORATORIUM"> PENGEMBANGAN II LABORATORIUM </option>
			    </select>
			</div>
		</div>
        <div class="form-group row">
			<div class="col-sm-8">
				<input type="text" class="form-control" name="jadlab" placeholder="NAMA LENGKAP"  required>
                <select name="jjadlab" class="form-control" required>
                    <option selected disabled>....</option>
					<option value="KEPALA LABORATORIUM"> KEPALA LABORATORIUM </option>
					<option value="KORDINATOR LABORATORIUM"> KORDINATOR LABORATORIUM </option>
					<option value="SEKRETARIS LABORATORIUM"> SEKRETARIS LABORATORIUM </option>
					<option value="PENJADWALAN LABORATORIUM"> PENJADWALAN LABORATORIUM </option>
					<option value="HUMAS I LABORATORIUM"> HUMAS I LABORATORIUM </option>
					<option value="HUMAS II LABORATORIUM"> HUMAS II LABORATORIUM </option>
					<option value="PENGEMBANGAN I LABORATORIUM"> PENGEMBANGAN I LABORATORIUM </option>
					<option value="PENGEMBANGAN II LABORATORIUM"> PENGEMBANGAN II LABORATORIUM </option>
			    </select>
			</div>
		</div>
        <div class="form-group row">
			<div class="col-sm-8">
				<input type="text" class="form-control" name="huslab1" placeholder="NAMA LENGKAP"  required>
                <select name="jhuslab1" class="form-control" required>
                    <option selected disabled>....</option>
					<option value="KEPALA LABORATORIUM"> KEPALA LABORATORIUM </option>
					<option value="KORDINATOR LABORATORIUM"> KORDINATOR LABORATORIUM </option>
					<option value="SEKRETARIS LABORATORIUM"> SEKRETARIS LABORATORIUM </option>
					<option value="PENJADWALAN LABORATORIUM"> PENJADWALAN LABORATORIUM </option>
					<option value="HUMAS I LABORATORIUM"> HUMAS I LABORATORIUM </option>
					<option value="HUMAS II LABORATORIUM"> HUMAS II LABORATORIUM </option>
					<option value="PENGEMBANGAN I LABORATORIUM"> PENGEMBANGAN I LABORATORIUM </option>
					<option value="PENGEMBANGAN II LABORATORIUM"> PENGEMBANGAN II LABORATORIUM </option>
			    </select>
			</div>
		</div>
        <div class="form-group row">
			<div class="col-sm-8">
				<input type="text" class="form-control" name="huslab2" placeholder="NAMA LENGKAP"  required>
                <select name="jhuslab2" class="form-control" required>
                    <option selected disabled>....</option>
					<option value="KEPALA LABORATORIUM"> KEPALA LABORATORIUM </option>
					<option value="KORDINATOR LABORATORIUM"> KORDINATOR LABORATORIUM </option>
					<option value="SEKRETARIS LABORATORIUM"> SEKRETARIS LABORATORIUM </option>
					<option value="PENJADWALAN LABORATORIUM"> PENJADWALAN LABORATORIUM </option>
					<option value="HUMAS I LABORATORIUM"> HUMAS I LABORATORIUM </option>
					<option value="HUMAS II LABORATORIUM"> HUMAS II LABORATORIUM </option>
					<option value="PENGEMBANGAN I LABORATORIUM"> PENGEMBANGAN I LABORATORIUM </option>
					<option value="PENGEMBANGAN II LABORATORIUM"> PENGEMBANGAN II LABORATORIUM </option>
			    </select>
			</div>
		</div>
        <div class="form-group row">
			<div class="col-sm-8">
				<input type="text" class="form-control" name="pengembang1" placeholder="NAMA LENGKAP"  required>
                <select name="jpengembang1" class="form-control" required>
                    <option selected disabled>....</option>
					<option value="KEPALA LABORATORIUM"> KEPALA LABORATORIUM </option>
					<option value="KORDINATOR LABORATORIUM"> KORDINATOR LABORATORIUM </option>
					<option value="SEKRETARIS LABORATORIUM"> SEKRETARIS LABORATORIUM </option>
					<option value="PENJADWALAN LABORATORIUM"> PENJADWALAN LABORATORIUM </option>
					<option value="HUMAS I LABORATORIUM"> HUMAS I LABORATORIUM </option>
					<option value="HUMAS II LABORATORIUM"> HUMAS II LABORATORIUM </option>
					<option value="PENGEMBANGAN I LABORATORIUM"> PENGEMBANGAN I LABORATORIUM </option>
					<option value="PENGEMBANGAN II LABORATORIUM"> PENGEMBANGAN II LABORATORIUM </option>
			    </select>
			</div>
		</div>
        <div class="form-group row">
			<div class="col-sm-8">
				<input type="text" class="form-control" name="pengembang2" placeholder="NAMA LENGKAP"  required>
                <select name="jpengembang2" class="form-control" required>
                    <option selected disabled>....</option>
					<option value="KEPALA LABORATORIUM"> KEPALA LABORATORIUM </option>
					<option value="KORDINATOR LABORATORIUM"> KORDINATOR LABORATORIUM </option>
					<option value="SEKRETARIS LABORATORIUM"> SEKRETARIS LABORATORIUM </option>
					<option value="PENJADWALAN LABORATORIUM"> PENJADWALAN LABORATORIUM </option>
					<option value="HUMAS I LABORATORIUM"> HUMAS I LABORATORIUM </option>
					<option value="HUMAS II LABORATORIUM"> HUMAS II LABORATORIUM </option>
					<option value="PENGEMBANGAN I LABORATORIUM"> PENGEMBANGAN I LABORATORIUM </option>
					<option value="PENGEMBANGAN II LABORATORIUM"> PENGEMBANGAN II LABORATORIUM </option>
			    </select>
			</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-10" style="float: right;">	
				<button class="btn btn-lg btn-danger" name="batal">BATAL</button>
				<button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
			</div>	
		</div>
	</div>
</form>
	<?php 
	include "koneksi.php";
	if (isset($_POST['tambah'])) 
	{
			$tahun = $_POST['tahun'];

			$kalab = $_POST['kalab'];
			$jkalab = $_POST['jkalab'];

			$koor = $_POST['koor'];
			$jkoor = $_POST['jkoor'];

			$seklab = $_POST['seklab'];
			$jseklab = $_POST['jseklab'];

			$jadlab = $_POST['jadlab'];
			$jjadlab = $_POST['jjadlab'];

			$huslab1 = $_POST['huslab1'];
			$jhuslab1 = $_POST['jhuslab1'];

			$huslab2 = $_POST['huslab2'];
			$jhuslab2 = $_POST['jhuslab2'];

			$pengembang1 = $_POST['pengembang1'];
			$jpengembang1 = $_POST['jpengembang1'];
			
			$pengembang2 = $_POST['pengembang2'];
			$jpengembang2 = $_POST['jpengembang2'];

            
			// jk foto diubah
		
		
			$kon = mysqli_query($koneksi, "INSERT INTO struktur 
			(nama, tahun_jabatan, jabatan)
			VALUES 
			('$kalab', '$tahun', '$jkalab'), 
			('$koor', '$tahun', '$jkoor'),
			('$seklab', '$tahun', '$jseklab'),
			('$jadlab', '$tahun', '$jjadlab'),
			('$huslab1', '$tahun', '$jhuslab1'),
			('$huslab2', '$tahun', '$jhuslab2'),
			('$pengembang1', '$tahun', '$jpengembang1'),
			('$pengembang2', '$tahun', '$jpengembang2')

			");
				
		echo "<script>alert('STRUKTUR LAB TELAH DITAMBAHKAN');</script>";
		echo "<meta http-equiv='refresh' content='1;url=index.php?halaman=struktur'> ";
	
	}

	?>