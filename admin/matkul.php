<?php
//library phpqrcode
include "phpqrcode/qrlib.php";
include 'koneksi.php';
 
//direktory tempat menyimpan hasil generate qrcode jika folder belum dibuat maka secara otomatis akan membuat terlebih dahulu
$tempdir = "temp/"; 
if (!file_exists($tempdir))
    mkdir($tempdir);
 
?>
<html>
<head>
</head>


<nav class="navbar navbar-light bg-light">
  
  <form class="form-inline">
  <a class="navbar-brand"><font size=5><b>JADWAL MATA KULIAH LABORATORIUM E-COMMERCE</font></a>
  <a href="index.php?halaman=tambah_matkul" class="btn btn-outline-primary"><img src="assets/img/tambah_data.png" width="32px">Tambah Data Matkul</a>
  <a href="cetak_jadwal.php" class="btn btn-outline-primary"><img src="assets/img/printer.png" width="32px">Cetak Jadwal </a>
  </form>
</nav>
<body>
  

    <table class="table table-striped table-dark" border="3" align="center">
        <thead>
            <th><center>KODE HARI<center></th>
            <th><center>HARI<center></th>
            <th><center>JAM<center></th>
            <th><center>MATKUL<center></th>
            <th><center>DOSEN</center></th>
            <th><center>TINDAKAN<center></th>
            <th><center>QR CODE<center></th>
            <th><center>SCAN QR<center></th>
            <th><center>ABSENSI<center></th>      
        </thead>
        <tbody>
        <?php
            $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
            $perpage = 8;
           $limit = ($page - 1) * $perpage;
           $prev = 6;
           $next = 1;
           $start_page = ($page - $prev) < 1 ? 1 : ($page - $prev);
           $sql = "SELECT * FROM matkul WHERE tahun_ajaran='2019-2020' ORDER BY kode_matkul ASC";
           $rs = mysqli_query($koneksi, $sql);
           $record = mysqli_num_rows($rs);
           $total_page = ceil($record / $perpage);
           $display_page = $start_page + $prev + $next;
           if($display_page > $total_page){
           $display_page = $total_page;
           }
           $sql .= ' LIMIT '.$limit.','.$perpage;
           $rs = mysqli_query($koneksi, $sql);
           

            while ($row = mysqli_fetch_assoc($rs)) {
                $kode_mk = $row['kode_matkul'];
                $dosen = $row['dosen'];
                $matkul = $row['matkul'];
                
                $hari = $row['hari'];
                $jam = $row['jam'];
                $tahun = $row['tahun_ajaran'];
                $w = $kode_mk . " " . $matkul . "  " . $hari . " " . $jam . " " . $tahun . " " .$dosen ;
                //Isi dari QRCode Saat discan
                $isi_teks1 = $w;
                //Nama file yang akan disimpan pada folder temp 
                $namafile1 = $w.".png";
                //Kualitas dari QRCode 
                $quality1 = 'H'; 
                //Ukuran besar QRCode
                $ukuran1 = 4; 
                $padding1 = 0; 
                QRCode::png($isi_teks1,$tempdir.$namafile1,$quality1,$ukuran1,$padding1);
        ?>
            <tr>
                <td><?php echo $row['kode_matkul']; ?></td>
                <td><?php echo $row['hari']; ?></td>
                <td><?php echo $row['jam']; ?></td>
                <td><?php echo $row['matkul']; ?></td>
                <td><?php echo $row['dosen']; ?></td>     
                <td>
                    <div class="btn-group" role="group" aria-label="Button group with nested dropdown">   
                    <div class="btn-group" role="group">
                        <a id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="assets/img/tindakan.png" width="32px">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <a class="dropdown-item btn btn-outline-primary" href="index.php?halaman=hapusmatkul&ni=<?php echo $row['kode_matkul'] ; ?>" ><img src="assets/img/delete.png" width="32px"> Hapus Matkul</a><br>
                            <a class="dropdown-item btn btn-outline-primary" href="index.php?halaman=ubahmatkul&ni=<?php echo $row['kode_matkul'] ; ?>"><img src="assets/img/edit.png" width="32px"> Edit Matkul</a><br>
                            <a class="dropdown-item btn btn-outline-primary" href="index.php?halaman=viewbarcode&ni=<?php echo $row['kode_matkul'] ; ?>"><img src="assets/img/view.png" width="32px"> View Matkul</a>
                        </div>
                    </div>
                </div>
                </td>  
                <td>
                <a href="view_barcode_matkul_dosen.php?ni=<?php echo $row['kode_matkul'] ; ?>" ><img src="temp/<?php echo $namafile1; ?>" width="32px"></a>
                </td>
                <td>
                <a href="test_barcode.php?ni=<?php echo $row['kode_matkul'] ; ?>" ><img src="assets/img/scan.jpg" width="32px"></a>
                </td>
                <td>
                <a href="kehadiran_dosen.php?ni=<?php echo $row['kode_matkul'] ; ?>"><img src="assets/img/absen.png" width="32px"></a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

             <?php
				  	$paging = null;
					if($total_page > 1){
					   	$paging .= '<ul class="pagination">';
				  			if($page > ($prev + 1)){
				   				$paging .= '<li><a href="index.php?halaman=daftar_matkul&page=1">first</a></li>';
				    			$paging .= '<li><a href="index.php?halaman=daftar_matkul&page='.($page - 1).'">prev</a></li>';
				  			}	
							for($i=$start_page; $i<=$display_page; $i++){
								if($i == $page){
									$paging .= '<li><a href="#'.$i.'">'.$i.'</a></li>';
								}else{
									$paging .= '<li><a href="index.php?halaman=daftar_matkul&page='.$i.'">'.$i.'</a></li>';
								}
							}
							if($total_page > $display_page){
								$paging .= '<li><a href="index.php?halaman=daftar_matkul&page='.($page + 1).'">next</a></li>';
								$paging .= '<li><a href="index.php?halaman=daftar_matkul&page='.$total_page.'">last</a></li>';
							}
				   			$paging .= '<ul>';
                  }
                  
                 echo "URUTAN HARI DALAM SATU MINGGU <br>";
				 echo $paging;
				 ?>


</body>
</html>
<?php mysqli_close($koneksi); ?>