<?php 
// session_start();
// if(empty($_SESSION['status_login'])){
// 	header('location:login.php');
// }
	include "koneksi.php"; 
	session_start(); 
	date_default_timezone_set('Asia/Jakarta');
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>LAB E-COMMERCE</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="shortcut icon" href="assets/img/logo.jpg">
   	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<!-- TOP NAV -->
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">E-COMMERCE</a> 
            </div>
            <div style="color: white; padding: 15px 20px 5px 20px; float: left; font-size: 20px; width: 670px;">
            	
				<marquee direction="right" >
					<?php echo "Selamat Datang, " . $_SESSION['nama'].".";?>
				</marquee>
            </div>
		  	<div style="color: white; padding: 15px 30px 5px 20px; float: right; font-size: 16px;"> 
			<?php  echo  date("l, j F Y, H:i");  ?> &nbsp; &nbsp; &nbsp; &nbsp;
			
			</div>	
	    </nav>   
   		<!-- SIDE NAV -->
	    <nav class="navbar-default navbar-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav" id="main-menu">				
					<li><a href="index.php?halaman=home"><i class="fa fa-home fa-3x"></i> Home</a></li>
					<li><a href="index.php?halaman=struktur"><i class="fa fa-suitcase fa-3x"></i>Struktur Organisasi</a></li>
					<li><a href="index.php?halaman=reqruitment"><i class="fa fa-user fa-3x"></i>Rekruitment</a></li>
					<li>
						<a href="#"><i class="fa fa-book fa-3x"></i>
							Berita Acara / Perkuliahan<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="index.php?halaman=daftar_matkul"> Mata Kuliah</a>
								</li>
								<li>
									<a href="index.php?halaman=berita_acara"> Berita Acara</a>
								</li>
							
							</ul>
					</li>
					<li>
						<a href="#"><i class="fa fa-book fa-3x"></i>
							Surat Masuk / Keluar <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="index.php?halaman=surat_masuk"> Surat Masuk</a>
								</li>
								<li>
									<a href="index.php?halaman=surat_keluar"> Surat Keluar</a>
								</li>
							
							</ul>
					</li>
					<li><a href="index.php?halaman=akses"><i class="fa fa-key fa-3x"></i> Data Asisten</a></li> 
					<li><a href="login.php"><i class="fa fa-close fa-3x"></i> Logout</a></li>
				</ul>
			</div>     
	    </nav>
	    <!-- PAGE INNER  -->
	    <div id="page-wrapper" >
	        <div id="page-inner">        
			<?php 
				class home{	
					public function cekhome($hal){
						if (isset($hal)){
							if ($_GET['halaman']=='home') {
								include 'home.php';
							}		
						}
						
					}	              				
				}

				class struktur{	
					public function cekstruktur($hal){
						if (isset($hal)){
							if ($_GET['halaman']=='struktur') {
								include 'struktur_organisasi.php';
							}	
							else if ($_GET['halaman']=='tambah_struktur') {
								include 'tambah_struktur_organisasi.php';
							}	
							else if ($_GET['halaman']=='ubah_struktur') {
								include 'ubah_struktur_organisasi.php';
							}		
						}
						
					}	              				
				}

				class surat{	
					public function ceksurat($hal){
						if (isset($hal)){
							if ($_GET['halaman']=='surat_masuk') {
								include 'surat_masuk.php';
							}
							else if ($_GET['halaman']=='tambah_data_surat_masuk') {
								include 'tambah_data_surat_masuk.php';
							}	
							else if ($_GET['halaman']=='ubah_data_surat_masuk') {
								include 'ubah_data_surat_masuk.php';
							}	
							else if ($_GET['halaman']=='hapus_data_surat_masuk') {
								include 'hapus_data_surat_masuk.php';
							}	
							else if ($_GET['halaman']=='view_data_surat_masuk') {
								include 'view_surat_masuk.php';
							}		
							else if ($_GET['halaman']=='surat_keluar') {
								include 'surat_keluar.php';
							}		
							else if ($_GET['halaman']=='tambah_data_surat_keluar') {
								include 'tambah_data_surat_keluar.php';
							}
							else if ($_GET['halaman']=='ubah_data_surat_keluar') {
								include 'ubah_data_surat_keluar.php';
							}	
							else if ($_GET['halaman']=='hapus_data_surat_keluar') {
								include 'hapus_data_surat_keluar.php';
							}		
							else if ($_GET['halaman']=='view_data_surat_keluar') {
								include 'view_surat_keluar.php';
							}		
						}						
					}	              				
				}
				
				class reqruitment{	
					public function cekreqruitment($hal){
						if (isset($hal)){
							if ($_GET['halaman']=='reqruitment') {
								include 'rekuitment.php';
							}
							else if ($_GET['halaman']=='hapususer_rekruitment') {
								include 'hapus_data_rekruitment.php';
							}
							else if ($_GET['halaman']=='ubahuser_rekruitment') {
								include 'ubah_data_rekruitment.php';
							}
							else if ($_GET['halaman']=='viewuser_rekruitment') {
								include 'view_data_rekruitment.php';
							}
							else if ($_GET['halaman']=='tambah_admin_rekruitment') {
								include 'tambah_rekruitment.php';
							}
							else if ($_GET['halaman']=='pengumuman_rekruitment') {
								include 'pengumuan.php';
							}	
							
						}
						
					}	              				
				}


				class data_asisten{	
					public function cek_data_asisten($hal){
						if (isset($hal)){
							if ($_GET['halaman']=='akses') {
								include 'data_asisten.php';
							}
							else if ($_GET['halaman']=='tambah_admin') {
								include 'tambah_data_asisten.php';
							}		
							else if ($_GET['halaman']=='hapususer') {
								include 'hapus_data_asisten.php';
							}
							else if ($_GET['halaman']=='ubahuser') {
								include 'ubah_data_asisten.php';
							}
							else if ($_GET['halaman']=='viewuser') {
								include 'view_data_asisten.php';
							}
							
						}
						
					}	              				
				}

				class matkul{	
					public function cek_data_matkul($hal){
						if (isset($hal)){
							if ($_GET['halaman']=='daftar_matkul') {
								include 'matkul.php';
							}
							else if ($_GET['halaman']=='tambah_matkul') {
								include 'tambah_data_matkul.php';
							}		
							else if ($_GET['halaman']=='hapusmatkul') {
								include 'hapus_data_matkul.php';
							}
							else if ($_GET['halaman']=='ubahmatkul') {
								include 'ubah_data_matkul.php';
							}
							else if ($_GET['halaman']=='viewuser') {
								include 'view_data_matkul.php';
							}
							else if ($_GET['halaman']=='viewbarcode') {
								include 'view_barcode_matkul.php';
							}
							else if ($_GET['halaman']=='berita_acara') {
								include 'berita_acara.php';
							}
							else if ($_GET['halaman']=='hapus_berita_acara') {
								include 'hapus_berita_acara.php';
							}
							else if ($_GET['halaman']=='tambah_data_berita_acara') {
								include 'tambah_data_berita_acara.php';
							}
							else if ($_GET['halaman']=='cetak_jadwal') {
								include 'cetak_jadwal.php';
							}
							else if ($_GET['halaman']=='kehadiran_dosen') {
								include 'kehadiran_dosen.php';
							}
							
						}
						
					}	              				
				}

      					
				$hal = $_GET['halaman'];
		
				$matkul = new matkul();
				$matkul->cek_data_matkul($hal);

				$home = new home();
				$home->cekhome($hal);
				
				$reqruitment = new reqruitment();
				$reqruitment->cekreqruitment($hal);
				
				$data_asisten = new data_asisten();
				$data_asisten->cek_data_asisten($hal);

				$struktr = new struktur();
				$struktr->cekstruktur($hal);

				$surat = new surat();
				$surat->ceksurat($hal);

                ?>

                

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>	
	</body>
</html>
