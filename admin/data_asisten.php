<?php include "koneksi.php"; ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
				<!-- TABLE DEKORASI -->
				<div class="table-responsive">	
					<table class='table table-bordered'>
						<tr>
							<td>
								<form method="post" enctype="multipart/form-data" autocomplete="off">
									<div class="form-group">
										<label> CARI BERDASARKAN </label>
										<select name="kategori">
											<option selected disabled>....</option>
											<option value="1"> TAHUN MASUK MENJADI ASISTEN</option>
											<option value="2"> NPM </option>
											<option value="3"> PRODI </option>
										</select>
									<input type="text" class="form-control" name="input" size="70">
									</div>
										<button class="btn btn-info muted" name="ubah">CARI</button>
										<button class="btn btn-muted" name="reload">RELOAD</button>
								</form>
							</td>	
						<tr>
					</table>
				</div>
				<a href="index.php?halaman=tambah_admin" class="btn btn-primary btn-lg btn-block">Tambah Data Asisten</a>
				<div class="table-responsive">	
					<table class='table table-bordered'>
						<thead>
							<tr>
							<th>Nama</th>
							<th>NPM</th>
							<th>Tahun masuk</th>
							<th>Prodi</th>
							<th>Foto</th>
							<th>Aksi</th>					
							</tr>
						</thead>
						<tbody>
							<?php
								if (isset($_POST['reload'])) 
								{
									echo "<script>location='index.php?halaman=akses';</script> ";
								}
							?>
							<?php 
							if (isset($_POST['ubah'])) 
							{
								$kategori = $_POST['kategori'];
								$input = $_POST['input'];
								$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
						 		$perpage = 7;
								$limit = ($page - 1) * $perpage;
								$prev = 1;
								$next = 2;
								$start_page = ($page - $prev) < 1 ? 1 : ($page - $prev);
								$sql = 'SELECT * FROM admin';
								$rs = mysqli_query($koneksi, $sql);
								$record = mysqli_num_rows($rs);
								$total_page = ceil($record / $perpage);
								$display_page = $start_page + $prev + $next;
								if($display_page > $total_page){
								$display_page = $total_page;
								}
								$sql .= ' LIMIT '.$limit.','.$perpage;
								$rs = mysqli_query($koneksi, $sql);
								switch($kategori){
									case 1:	
										$rs = mysqli_query($koneksi, "SELECT * FROM admin WHERE tahun_masuk='$input'");
									break;
									case 2:		
										$rs = mysqli_query($koneksi, "SELECT * FROM admin WHERE npm='$input'");
									break;
									case 3:			
										$rs = mysqli_query($koneksi, "SELECT * FROM admin WHERE prodi='$input'");
									break;
								}
										
							}
							
							else{
						 		$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
						 		$perpage = 7;
								$limit = ($page - 1) * $perpage;
								$prev = 1;
								$next = 2;
								$start_page = ($page - $prev) < 1 ? 1 : ($page - $prev);
								$sql = 'SELECT * FROM admin';
								$rs = mysqli_query($koneksi, $sql);
								$record = mysqli_num_rows($rs);
								$total_page = ceil($record / $perpage);
								$display_page = $start_page + $prev + $next;
								if($display_page > $total_page){
								$display_page = $total_page;
								}
								$sql .= ' LIMIT '.$limit.','.$perpage;
								$rs = mysqli_query($koneksi, $sql);
								}
							 ?>
							
							<?php while($data = mysqli_fetch_assoc($rs)){ ?>
								<tr>
								<td width="50"><?php echo $data['nama'] ; ?></td>
								<td width="50"><?php echo $data['npm'] ; ?></td>
								<td width="32"><?php echo $data['tahun_masuk'] ; ?></td>
								<td width="30"><?php echo $data['prodi'] ; ?></td>
								<td width="60"><img src="../surat/foto_asisten/<?php echo $data['foto'] ;?>" width="50"></td>
								<td  width="106">
									<a href="index.php?halaman=hapususer&ni=<?php echo $data['npm'] ; ?> " class="btn btn-danger" onClick="return confirm('Apakah anda yakin data ini akan di hapus secara permanen?');">Hapus</a>
									<a href="index.php?halaman=ubahuser&ni=<?php echo $data['npm'] ; ?>" class="btn btn-warning">Edit</a>
									<a href="index.php?halaman=viewuser&ni=<?php echo $data['npm'] ; ?>" class="btn btn-info">View</a>
								</td>
								</tr>
							<?php } ?>
							
						</tbody>
					</table>
				</div>
				<?php
				  	$paging = null;
					if($total_page > 1){
					   	$paging .= '<ul class="pagination">';
				  			if($page > ($prev + 1)){
				   				$paging .= '<li><a href="index.php?halaman=akses&page=1">first</a></li>';
				    			$paging .= '<li><a href="index.php?halaman=akses&page='.($page - 1).'">prev</a></li>';
				  			}	
							for($i=$start_page; $i<=$display_page; $i++){
								if($i == $page){
									$paging .= '<li><a href="#'.$i.'">'.$i.'</a></li>';
								}else{
									$paging .= '<li><a href="index.php?halaman=akses&page='.$i.'">'.$i.'</a></li>';
								}
							}
							if($total_page > $display_page){
								$paging .= '<li><a href="index.php?halaman=akses&page='.($page + 1).'">next</a></li>';
								$paging .= '<li><a href="index.php?halaman=akses&page='.$total_page.'">last</a></li>';
							}
				   			$paging .= '<ul>';
				  }
				 echo $paging;
				 ?>
            </div>
        
	</body>
</html>
