<h2> SELAMAT DATANG DI WEBSITE LABORATORIUM E-COMMERCE </h2>

<br>
<pre><h5><b>Tujuan pembuatan website ini agar memudahkan
penjadwalan dan sekretaris dalam menginput data
dan menghemat kertas agar dokumen tidak numpuk di lab</b></h5></pre>

<pre><h5><b>Hal yang harus diperhatikan
  1. Fitur diwebsite ini disetting sampai tahun 2024
  2. Setiap semester harus dirubah sedikit script nya
     dibagian query untuk menampilkan data mata kuliah
     Contoh : select * from matkul where tahun_ajaran='2019-2020'
  3. Upload dan download file PHP dan Database di web hosting
     setidaknya 4 atau 5 kali dalam satu semester untuk mencegah
     hilangnya data
  4. File PHP dan Database yang didownload dari web hosting
     simpan pada pc aslab 2, untuk pembukuan digital
  5. Jika ingin mengembangkan website ini, ada di pc aslab 2
     </b></h5></pre>

<pre><h5><center><b>Hormat Kami
Asisten Laboratorium E-commerce
Tahun Priode 2019-2020</center></b></h5></pre>