-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 25, 2019 at 06:17 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lab_ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `npm` bigint(15) NOT NULL,
  `password` varchar(12) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `prodi` enum('TI','SI') NOT NULL,
  `tahun_masuk` int(4) NOT NULL,
  `about` text NOT NULL,
  `foto` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`npm`, `password`, `nama`, `prodi`, `tahun_masuk`, `about`, `foto`) VALUES
(1, 'L4B1NF0S4TU', 'Asisten Laboratorium', 'TI', 2019, '1', '1'),
(173112706450221, 'lupa', 'Danang Aji Pangestu', 'TI', 2019, '', 'me.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `berita_acara`
--

CREATE TABLE `berita_acara` (
  `nama` text NOT NULL,
  `tanggal` text NOT NULL,
  `kode_matkul` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita_acara`
--

INSERT INTO `berita_acara` (`nama`, `tanggal`, `kode_matkul`) VALUES
('QR Code: Agus Iskandar, S.Kom., M.Kom.  1021920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 25 September 2019, 10:39', 1021920),
('QR Code: Novi Dian Nathasia, S.Kom, MMSI  1031920 Sistem Basis Data   2019-2020', 'Wednesday, 25 September 2019, 10:39', 1031920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  1051920 Praktikum Sistem Operasi  2019-2020', 'Wednesday, 25 September 2019, 10:39', 1051920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  1071920 Praktikum Sistem Basis Data  2019-2020', 'Wednesday, 25 September 2019, 10:40', 1071920),
('QR Code: Benrahmaf, S.Kom, M.MSI  1081920 Praktikum Pemrograman Multimedia  2019-2020', 'Wednesday, 25 September 2019, 10:40', 1081920),
('QR Code:  Yunan Fauzi Wijaya, S.Kom.,MMSI.  2011920 Grafik Komputer  2019-2020', 'Wednesday, 25 September 2019, 10:41', 2011920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2021920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 25 September 2019, 10:41', 2021920),
('QR Code: Arie Gunawan, S.Kom, MMSI  2031920 Praktikum SIstem Operasi  2019-2020', 'Wednesday, 25 September 2019, 10:42', 2031920),
('QR Code: Novi Dian Nathasia, S.Kom.,MMSi  2051920 Perancangin Basis Data Lanjut  2019-2020', 'Wednesday, 25 September 2019, 10:42', 2051920),
('QR Code: Dr. Fauziah, S Kom. , M.M.S.I.  2061920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 25 September 2019, 10:43', 2061920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2071920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 25 September 2019, 10:44', 2071920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  2081920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 25 September 2019, 10:45', 2081920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  4011920 Praktikum Pemrog Multimedia  2019-2020', 'Thursday, 26 September 2019, 08:55', 4011920),
('QR Code:  Rima Tamara Aldisa, S.Kom., M.Kom.  4021920 Praktikum Sistem Operasi  2019-2020', 'Thursday, 26 September 2019, 10:06', 4021920),
('QR Code: Sigit Wijanarko S.T, M.TI  4031920 Data Science  2019-2020', 'Thursday, 26 September 2019, 12:12', 4031920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  4051920 Praktikum Sistem Operasi  2019-2020', 'Thursday, 26 September 2019, 16:40', 1051920),
('QR Code: Nur Hayati, S.Si., M.T.I.  4071920 Analisa Proses Bisnis  2019-2020', 'Friday, 27 September 2019, 08:04', 4071920),
('QR Code: Benrahman, S.Kom, M.MSI  4081920 Deep Learning  2019-2020', 'Friday, 27 September 2019, 08:05', 4081920),
('QR Code: Yunan Fauzi Wijaya, S>Kom.,MMSI.  5011920 Praktikum Pemrog Multimedia  2019-2020', 'Friday, 27 September 2019, 13:29', 5011920),
('QR Code:  Winarsih, S.Si., MMSI  1011920 Praktikum Sistem Basis Data  2019-2020', 'Tuesday, 8 October 2019, 13:30', 1011920),
('QR Code: Agus Iskandar, S.Kom., M.Kom.  1021920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Tuesday, 8 October 2019, 13:31', 1021920),
('QR Code: Novi Dian Nathasia, S&Kom, MMSI  1031920 Sistem Basis Data   2019-2020', 'Tuesday, 8 October 2019, 13:31', 1031920),
('QR Code:  Winarsih, S.Si., MMSI  1041920 Praktikum Sistem Basis Data  2019-2020', 'Tuesday, 8 October 2019, 13:32', 1041920),
('QR Code:  Winarsih, S.Si., MMSI  1061920 Praktikum Pemrograman Visual  2019-2020', 'Tuesday, 8 October 2019, 13:32', 1061920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  1071920 Praktikum Sistem Basis Data  2019-2020', 'Tuesday, 8 October 2019, 13:33', 1071920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2021920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Tuesday, 8 October 2019, 13:34', 2021920),
('QR Code: Arie Gunawan, S.Kom, MMSI  2031920 Praktikum SIstem Operasi  2019-2020', 'Tuesday, 8 October 2019, 13:34', 2031920),
('QR Code: Benrahman, S.Kom, M.MSI  2041920 Praktikum Pemrograman Visual  2019-2020', 'Tuesday, 8 October 2019, 15:14', 2041920),
('QR Code: Novi Dian Nathasia, S.Kom.,MMSi  2051920 Perancangan Basis Data Lanjut  2019-2020', 'Tuesday, 8 October 2019, 17:10', 2051920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2061920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Tuesday, 8 October 2019, 19:30', 2061920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2071920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Tuesday, 8 October 2019, 20:59', 2071920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  4011920 Praktikum Pemrog Multimedia  2019-2020', 'Thursday, 10 October 2019, 09:52', 4011920),
('Code93: AK', 'Thursday, 10 October 2019, 10:13', 4021920),
('QR Code: Moh. Iwan Wahyuddin, S.T., M.T.  4041920 Praktikum Sistem Operasi  2019-2020', 'Thursday, 10 October 2019, 15:09', 4041920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  5011920 Praktikum Pemrog Multimedia  2019-2020', 'Friday, 11 October 2019, 10:00', 5011920),
('QR Code:  Winarsih, S.Si., MMSI  1011920 Praktikum Sistem Basis Data  2019-2020', 'Wednesday, 23 October 2019, 09:42', 1011920),
('QR Code: Agus Iskandar, S.Kom., M.Kom.  1021920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 23 October 2019, 09:43', 1021920),
('QR Code: Novi Dian Nathasia, S.Kom, MMSI  1031920 Sistem Basis Data   2019-2020', 'Wednesday, 23 October 2019, 09:43', 1031920),
('QR Code:  Winarsih, S.Si., MMSI  1041920 Praktikum Sistem Basis Data  2019-2020', 'Wednesday, 23 October 2019, 09:44', 1041920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2021920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 23 October 2019, 09:45', 2021920),
('QR Code: Novi Dian Nathasia, S.Kom.,MMSi  2051920 Perancangan Basis Data Lanjut  2019-2020', 'Wednesday, 23 October 2019, 09:46', 2051920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2061920 PRaktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 23 October 2019, 09:46', 2061920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2071920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 23 October 2019, 09:47', 2071920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  4051920 Praktikum Sistem Operasi  2019-2020', 'Wednesday, 23 October 2019, 10:20', 1051920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  4051920 Praktikum Sistem Operasi  2019-2020', 'Wednesday, 23 October 2019, 10:34', 4051920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  4051920 Praktikum Sistem Operasi  2019-2020', 'Wednesday, 23 October 2019, 10:34', 4051920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  4051920 Praktykum Sistem Operasi  2019-2020', 'Wednesday, 23 October 2019, 10:34', 4051920),
('QR Code: Novi Dian Nathasia, S.Kom.,MMSi  4061920 Perancangan Basis Data Lanjut  2019-2020', 'Wednesday, 23 October 2019, 10:35', 4061920),
('QR Code: Novi Dian Nathasia, S.Kom.,MMSi  4061920 Perancangan Basis Data Lanjut  2019-2020', 'Wednesday, 23 October 2019, 10:35', 4061920),
('QR Code: Novi Dian Nathasia, S.Kom.,MMSi  4061920 Perancangan Basis Data Lanjut  2019-2020', 'Wednesday, 23 October 2019, 10:35', 4061920),
('QR Code: Nur Hayati, S.Si., M.T.I.  4071920 Analisa Proses Bisnis  2019-2020', 'Wednesday, 23 October 2019, 10:36', 4071920),
('QR Code: Nur Hayati, S.Si., M.T.I.  5041920 Praktikum E-Business  2019-2020', 'Wednesday, 23 October 2019, 10:38', 5041920),
('QR Code: Nur Hayati, S.Si., M.T.I.  3031920 Praktikum E-Business  2019-2020', 'Wednesday, 23 October 2019, 15:38', 3031920),
('QR Code: Nur Hayati, S.Si., M.T.I.  3031920 Praktikum E-Business  2019-2020', 'Wednesday, 23 October 2019, 15:38', 3031920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  5051920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 23 October 2019, 15:49', 5051920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  5051920 Praktikum Dasar - Dasar Peirograman I  2019-2020', 'Wednesday, 23 October 2019, 15:50', 5051920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  5051920 Praktikum Dascr - Dasar Pemrograman I  2019-2020', 'Wednesday, 23 October 2019, 15:50', 5051920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  6021920 Praktikum Sistem Operasi  2019-2020', 'Wednesday, 23 October 2019, 15:51', 6021920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  6021920 Praktikum Sistem Operasi  2019-2020', 'Wednesday, 23 October 2019, 15:51', 6021920),
('QR Code: Nur Hayati, S.Si., M.T.I.  6031920 Analisa Proses Bisnis  2019-2020', 'Wednesday, 23 October 2019, 15:52', 6031920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  3061920 Praktikum E-Business  2019-2020', 'Friday, 25 October 2019, 10:16', 3061920),
('QR Code:  Rima Tamara Aldisa, S.Kom., M.Kom.  4021920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 10:17', 4021920),
('QR Code: Nur Hayati, S.Si., M.T.I.  5041920 Praktikum E-Business  2019-2020', 'Friday, 25 October 2019, 10:22', 5041920),
('QR Code:  Winarsih, S.Si., MMSI  1011920 Praktikum Sistem Basis Data  2019-2020', 'Friday, 25 October 2019, 10:50', 1011920),
('QR Code:  Winarsih, S.Si., MMSI  1061920 Praktikum Pemrograman Visual  2019-2020', 'Friday, 25 October 2019, 10:51', 1061920),
('QR Code:  Winarsih, S.Si., MMSI  1061920 Praktikum Pemrograman Visual  2019-2020', 'Friday, 25 October 2019, 10:51', 1061920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  1071920 Praktikum Sistem Basis Data  2019-2020', 'Friday, 25 October 2019, 10:52', 1071920),
('QR Code: Benrahman, S.Kom, M.MSI  1081920 Praktikum Pemrograman Multimedia  2019-2020', 'Friday, 25 October 2019, 10:53', 1081920),
('QR Code: Benrahman, S.Kom, M.MSI  1081920 Praktikum Pemrograman Multimedia  2019-2020', 'Friday, 25 October 2019, 10:53', 1081920),
('QR Code:  Yunan Fauzi Wijaya, S.Kom.,MMSI.  2011920 Grefik Komputer  2019-2020', 'Friday, 25 October 2019, 10:54', 2011920),
('QR Code:  Yunan Fauzi Wijaya, S.Kom.,MMSI.  2011920 Grafik Komputer  2019-2020', 'Friday, 25 October 2019, 10:54', 2011920),
('QR Code: Arie Gunawan, S.Kom, MMSI  2031920 Praktikum SIstem Operasi  2019-2020', 'Friday, 25 October 2019, 10:54', 2031920),
('QR Code: Benrahman, S.Kom, M.MSI  2041920 Praktikum Pemrograman Visual  2019-2020', 'Friday, 25 October 2019, 10:55', 2041920),
('QR Code: Benrahman, S.Kom, M.MSI  2041920 Praktikum Pemrograman Visual  2019-2020', 'Friday, 25 October 2019, 10:56', 2041920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  2081920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Friday, 25 October 2019, 10:57', 2081920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  2081920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Friday, 25 October 2019, 10:57', 2081920),
('QR Code:  Winarsih, S.Si., MMSI  3011920 Basis Data  2019-2020', 'Friday, 25 October 2019, 10:58', 3011920),
('QR Code:  Winarsih, S.Si., MMSI  3011924 Basis Data  2019-2020', 'Friday, 25 October 2019, 10:58', 3011920),
('QR Code:  Winarsih, S.Si., MMSI  3011920 Basis Data  2019-2020', 'Friday, 25 October 2019, 10:58', 3011920),
('QR Code: Winarsih, S.SI., MMSI  3021920 Basis Data  2019-2020', 'Friday, 25 October 2019, 10:59', 3021920),
('QR Code: Winarsih, S.SI., MMSI  3021920 Basis Data  2019-2020', 'Friday, 25 October 2019, 10:59', 3021920),
('QR Code: Winarsih, S.SI., MMSI  3021920 Basis Data  2019-2020', 'Friday, 25 October 2019, 10:59', 3021920),
('QR Code: Nur Hayati, S.Si., M.T.I.  3031920 Praktikum E-Business  2019-2020', 'Friday, 25 October 2019, 11:00', 3031920),
('QR Code:  Winarsih, S.Si., MMSI  3041920 Sistem Basis Data  2019-2020', 'Friday, 25 October 2019, 11:00', 3041920),
('QR Code:  Winarsih, S.Si., MMSI  3041920 Sistem Basis Data  2019-2020', 'Friday, 25 October 2019, 11:00', 3041920),
('QR Code:  Winarsih,\0S.Si.,DXMSI  3041920 Sistem Basis Data  2019-2020', 'Friday, 25 October 2019, 11:00', 3041920),
('QR Code: Winarsih, S.SI., MMSI  3051920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:01', 3051920),
('QR Code: Winarsih, S.SI., MMSI  3051920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:01', 3051920),
('QR Code: Winarsih, S.SI., MMSI  3051920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:01', 3051920),
('QR Code: Yunan Fauzi Wijaya, S.Knm.,MMSI.  3061920 Praktikum E-Business  2019-2020', 'Friday, 25 October 2019, 11:02', 3061920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  3061920 Praktikum E-Business  2019-2020', 'Friday, 25 October 2019, 11:02', 3061920),
('QR Code: Winarsih, S.SI., MMSI  3071920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:03', 3071920),
('QR Code: Winarsih, S.SI., MMSI  3071920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:03', 3071920),
('QR Code: Winarsih, S.SI., MMSI  3071920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:03', 3071920),
('QR Code: Winarsih, S.SI., MMSI  3081920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:04', 3081920),
('QR Code: Winarsih, S.SI., MMSI  3081920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:04', 3081920),
('QR Code: Winarsih, S.SI., MMSI  3081920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:04', 3081920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  4011920 Praktikum Pemrog Multimedia  2019-2020', 'Friday, 25 October 2019, 11:05', 4011920),
('QR Code: Sigit Wijanarko S.T, M.TI  4031920 Data Science  2019-2020', 'Friday, 25 October 2019, 11:05', 4031920),
('QR Code: Sigit Wijanarko S.T, M.TI  ', 'Friday, 25 October 2019, 11:05', 4031920),
('QR Code: Moh. Iwan Wahyuddin, S.T., M.T.  4041920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 11:06', 4041920),
('QR Code: Moh. Iwan Wahyuddin, S.T., M.T.  4041920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 11:06', 4041920),
('QR Code: Nur Hayati, S.Si., M.T.I.  4071920 Analisa Proses Bisnis  2019-2020', 'Friday, 25 October 2019, 11:07', 4071920),
('QR Code: Benrahman, S.Kom, M.MSI  4081920 Deep Learning  2019-2020', 'Friday, 25 October 2019, 11:08', 4081920),
('QR Code: Benrahman, S.Kom, M.MSI  4081920 Deep Learning  2019-2020', 'Friday, 25 October 2019, 11:08', 4081920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  5011920 Praktikum Pemrog Multimedia  2019-2020', 'Friday, 25 October 2019, 11:09', 5011920),
('QR Code: Gatot Supriyono, S.Si., M.S.M.  5021920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 11:10', 5021920),
('QR Code: Gatot Supriyono, S.Si., M.S.M.  5021920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 11:10', 5021920),
('QR Code: Gatot Supriyono, S.Si., M.S.M.  5021920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 11:10', 5021920),
('QR Code: Nur Hayati, S.Si., M.T.I.  5041920 Praktikum E-Business  2019-2020', 'Friday, 25 October 2019, 11:11', 5041920),
('QR Code: Agung Triayudi S.kom., M.Kom  5061920 Rekayasa Perangkat Lunak Sistem Informasi  2019-2020', 'Friday, 25 October 2019, 11:12', 5061920),
('QR Code: Agung Triayudi S.kom., M.Kom  5061920 Rekayasa Perangkat Lunak Sistem Informasi  2019-2020', 'Friday, 25 October 2019, 11:12', 5061920),
('QR Code: Agung Triayudi S.kom., M.Kom  5061920 Rekayasa Perangkat Lunak Sistem Informasi  2019-2020', 'Friday, 25 October 2019, 11:12', 5061920),
('QR Code: Agung Triayudi S.kom., M.Kom  5071920 Basis Daua  2019-2020', 'Friday, 25 October 2019, 11:13', 5071920),
('QR Code: Agung Triayudi S.kom., M.Kom  5071920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:13', 5071920),
('QR Code: Agung Triayudi S.kom., M.Kom  5071920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:13', 5071920),
('QR Code: Agung Triayudi S.kom., M.Kom  5081920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:14', 5081920),
('QR Code: Agung Triayudi S.kom., M.Kom  5081920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:14', 5081920),
('QR Code: Agung Triayudi S.kom., M.Kom  5081920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:14', 5081920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  6021920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 11:15', 6021920),
('QR Code: Nur Hayati, S.Si., M.T.I.  6031920 Analisa Proses Bisnis  2019-2020', 'Friday, 25 October 2019, 11:15', 6031920),
('QR Code: Nur Hayati, S.Si., M.T.I.  6031920 Analisa Proses Bisnis  2019-2020', 'Friday, 25 October 2019, 11:15', 6031920),
('QR Code: Winarsih, S.Si., MMSI  6041920 Praktikum Sistem B`sis Data  2019-2020', 'Friday, 25 October 2019, 11:16', 6041920),
('QR Code: Winarsih, S.Si., MMSI  6041920 Praktikum Sistem Basis Data %2019-2020', 'Friday, 25 October 2019, 11:16', 6041920),
('QR Code: Winarshx, S.Si., MMSI  6041920 Praktikum Sistem Basis Data  2019-2020', 'Friday, 25 October 2019, 11:16', 6041920);

-- --------------------------------------------------------

--
-- Table structure for table `matkul`
--

CREATE TABLE `matkul` (
  `kode_matkul` int(20) NOT NULL,
  `matkul` varchar(80) NOT NULL,
  `hari` enum('SENIN','SELASA','RABU','KAMIS','JUMAT','SABTU') NOT NULL,
  `jam` enum('08.00-09.40','09.50-11.30','11.40-13.20','13.30-15.10','15.20-17.00','17.00-18.40','18.50-20.20','20.20-22.00') NOT NULL,
  `dosen` varchar(90) NOT NULL,
  `tahun_ajaran` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matkul`
--

INSERT INTO `matkul` (`kode_matkul`, `matkul`, `hari`, `jam`, `dosen`, `tahun_ajaran`) VALUES
(1011920, 'Praktikum Sistem Basis Data', 'SENIN', '08.00-09.40', ' Winarsih, S.Si., MMSI', '2019-2020'),
(1021920, 'Praktikum Dasar - Dasar Pemrograman I', 'SENIN', '09.50-11.30', 'Agus Iskandar, S.Kom., M.Kom.', '2019-2020'),
(1031920, 'Sistem Basis Data ', 'SENIN', '11.40-13.20', 'Novi Dian Nathasia, S.Kom, MMSI', '2019-2020'),
(1041920, 'Praktikum Sistem Basis Data', 'SENIN', '13.30-15.10', ' Winarsih, S.Si., MMSI', '2019-2020'),
(1051920, 'Praktikum Sistem Operasi', 'SENIN', '15.20-17.00', 'Arie Gunawan, S.Kom., MMSI.', '2019-2020'),
(1061920, 'Praktikum Pemrograman Visual', 'SENIN', '17.00-18.40', ' Winarsih, S.Si., MMSI', '2019-2020'),
(1071920, 'Praktikum Sistem Basis Data', 'SENIN', '18.50-20.20', ' Agus Iskandar, S.Kom., M.Kom.', '2019-2020'),
(1081920, 'Praktikum Pemrograman Multimedia', 'SENIN', '20.20-22.00', 'Benrahman, S.Kom, M.MSI', '2019-2020'),
(2011920, 'Grafik Komputer', 'SELASA', '08.00-09.40', ' Yunan Fauzi Wijaya, S.Kom.,MMSI.', '2019-2020'),
(2021920, 'Praktikum Dasar - Dasar Pemrograman I', 'SELASA', '09.50-11.30', 'Dr. Fauziah, S. Kom. , M.M.S.I.', '2019-2020'),
(2031920, 'Praktikum SIstem Operasi', 'SELASA', '11.40-13.20', 'Arie Gunawan, S.Kom, MMSI', '2019-2020'),
(2041920, 'Praktikum Pemrograman Visual', 'SELASA', '13.30-15.10', 'Benrahman, S.Kom, M.MSI', '2019-2020'),
(2051920, 'Perancangan Basis Data Lanjut', 'SELASA', '15.20-17.00', 'Novi Dian Nathasia, S.Kom.,MMSi', '2019-2020'),
(2061920, 'Praktikum Dasar - Dasar Pemrograman I', 'SELASA', '17.00-18.40', 'Dr. Fauziah, S. Kom. , M.M.S.I.', '2019-2020'),
(2071920, 'Praktikum Dasar - Dasar Pemrograman I', 'SELASA', '18.50-20.20', 'Dr. Fauziah, S. Kom. , M.M.S.I.', '2019-2020'),
(2081920, 'Praktikum Dasar - Dasar Pemrograman I', 'SELASA', '20.20-22.00', ' Agus Iskandar, S.Kom., M.Kom.', '2019-2020'),
(3011920, 'Basis Data', 'RABU', '08.00-09.40', ' Winarsih, S.Si., MMSI', '2019-2020'),
(3021920, 'Basis Data', 'RABU', '09.50-11.30', 'Winarsih, S.SI., MMSI', '2019-2020'),
(3031920, 'Praktikum E-Business', 'RABU', '11.40-13.20', 'Nur Hayati, S.Si., M.T.I.', '2019-2020'),
(3041920, 'Sistem Basis Data', 'RABU', '13.30-15.10', ' Winarsih, S.Si., MMSI', '2019-2020'),
(3051920, 'Basis Data', 'RABU', '15.20-17.00', 'Winarsih, S.SI., MMSI', '2019-2020'),
(3061920, 'Praktikum E-Business', 'RABU', '17.00-18.40', 'Yunan Fauzi Wijaya, S.Kom.,MMSI.', '2019-2020'),
(3071920, 'Basis Data', 'RABU', '18.50-20.20', 'Winarsih, S.SI., MMSI', '2019-2020'),
(3081920, 'Basis Data', 'RABU', '20.20-22.00', 'Winarsih, S.SI., MMSI', '2019-2020'),
(4021920, 'Praktikum Sistem Operasi', 'KAMIS', '09.50-11.30', ' Rima Tamara Aldisa, S.Kom., M.Kom.', '2019-2020'),
(4031920, 'Data Science', 'KAMIS', '11.40-13.20', 'Sigit Wijanarko S.T, M.TI', '2019-2020'),
(4041920, 'Praktikum Sistem Operasi', 'KAMIS', '13.30-15.10', 'Moh. Iwan Wahyuddin, S.T., M.T.', '2019-2020'),
(4051920, 'Praktikum Sistem Operasi', 'KAMIS', '15.20-17.00', 'Arie Gunawan, S.Kom., MMSI.', '2019-2020'),
(4061920, 'Perancangan Basis Data Lanjut', 'KAMIS', '17.00-18.40', 'Novi Dian Nathasia, S.Kom.,MMSi', '2019-2020'),
(4071920, 'Analisa Proses Bisnis', 'KAMIS', '18.50-20.20', 'Nur Hayati, S.Si., M.T.I.', '2019-2020'),
(4011920, 'Praktikum Pemrog Multimedia', 'KAMIS', '08.00-09.40', 'Yunan Fauzi Wijaya, S.Kom.,MMSI.', '2019-2020'),
(4081920, 'Deep Learning', 'KAMIS', '20.20-22.00', 'Benrahman, S.Kom, M.MSI', '2019-2020'),
(5021920, 'Praktikum Sistem Operasi', 'JUMAT', '09.50-11.30', 'Gatot Supriyono, S.Si., M.S.M.', '2019-2020'),
(5011920, 'Praktikum Pemrog Multimedia', 'JUMAT', '08.00-09.40', 'Yunan Fauzi Wijaya, S.Kom.,MMSI.', '2019-2020'),
(5031920, 'SHALAT JUMAT', 'JUMAT', '11.40-13.20', 'Khotib Masjid', '2019-2020'),
(5041920, 'Praktikum E-Business', 'JUMAT', '13.30-15.10', 'Nur Hayati, S.Si., M.T.I.', '2019-2020'),
(5051920, 'Praktikum Dasar - Dasar Pemrograman I', 'JUMAT', '15.20-17.00', 'Dr. Fauziah, S. Kom. , M.M.S.I.', '2019-2020'),
(5061920, 'Rekayasa Perangkat Lunak Sistem Informasi', 'JUMAT', '17.00-18.40', 'Agung Triayudi S.kom., M.Kom', '2019-2020'),
(5071920, 'Basis Data', 'JUMAT', '18.50-20.20', 'Agung Triayudi S.kom., M.Kom', '2019-2020'),
(5081920, 'Basis Data', 'JUMAT', '20.20-22.00', 'Agung Triayudi S.kom., M.Kom', '2019-2020'),
(6021920, 'Praktikum Sistem Operasi', 'SABTU', '09.50-11.30', 'Arie Gunawan, S.Kom., MMSI.', '2019-2020'),
(6031920, 'Analisa Proses Bisnis', 'SABTU', '11.40-13.20', 'Nur Hayati, S.Si., M.T.I.', '2019-2020'),
(6041920, 'Praktikum Sistem Basis Data', 'SABTU', '13.30-15.10', 'Winarsih, S.Si., MMSI', '2019-2020'),
(6051920, '-', 'SABTU', '15.20-17.00', '-', '2019-2020'),
(6061920, '-', 'SABTU', '17.00-18.40', '-', '2019-2020'),
(6071920, '-', 'SABTU', '18.50-20.20', '-', '2019-2020'),
(6081920, '-', 'SABTU', '20.20-22.00', '-', '2019-2020'),
(6011920, '-', 'SABTU', '08.00-09.40', '-', '2019-2020');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `pengumuman` text NOT NULL,
  `kriteria` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `perkuliahan`
--

CREATE TABLE `perkuliahan` (
  `matkul` varchar(70) NOT NULL,
  `dosen` varchar(60) NOT NULL,
  `tahun_ajaran` varchar(90) NOT NULL,
  `dokumen` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `requitment`
--

CREATE TABLE `requitment` (
  `npm` bigint(15) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `prodi` enum('TI','SI') NOT NULL,
  `programing` text NOT NULL,
  `design` text NOT NULL,
  `networking` text NOT NULL,
  `cv` varchar(90) NOT NULL,
  `alasan` text NOT NULL,
  `about` text NOT NULL,
  `lolos` enum('LOLOS','TIDAK') NOT NULL,
  `tahun` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requitment`
--

INSERT INTO `requitment` (`npm`, `nama`, `prodi`, `programing`, `design`, `networking`, `cv`, `alasan`, `about`, `lolos`, `tahun`) VALUES
(2, '2', 'TI', '2', '2', '2', '11-40-1-PB (1).pdf', '2', '2', 'LOLOS', 2019);

-- --------------------------------------------------------

--
-- Table structure for table `struktur`
--

CREATE TABLE `struktur` (
  `nama` varchar(80) NOT NULL,
  `tahun_jabatan` int(5) NOT NULL,
  `jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `struktur`
--

INSERT INTO `struktur` (`nama`, `tahun_jabatan`, `jabatan`) VALUES
('ANDRIANINGSIH S.KOM M.KOM', 2019, 'KEPALA LABORATORIUM'),
('SHOFY NAQIAH', 2019, 'KORDINATOR LABORATORIUM'),
('SYIFA FARADILLA FABRIANNE', 2019, 'SEKRETARIS LABORATORIUM'),
('WINDA ANTIKA PUTRI', 2019, 'PENJADWALAN LABORATORIUM'),
('FACHRUL RAZI PRAWIRA', 2019, 'HUMAS I LABORATORIUM'),
('DANANG AJI PANGESTU', 2019, 'HUMAS II LABORATORIUM'),
('ALDY CANTONA', 2019, 'PENGEMBANGAN I LABORATORIUM'),
('MUHAMMAD RIDWAN', 2019, 'PENGEMBANGAN II LABORATORIUM');

-- --------------------------------------------------------

--
-- Table structure for table `surat_keluar`
--

CREATE TABLE `surat_keluar` (
  `nomer` int(10) NOT NULL,
  `perihal` varchar(60) NOT NULL,
  `tujuan` int(90) NOT NULL,
  `dokumen` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `surat_masuk`
--

CREATE TABLE `surat_masuk` (
  `nomer` varchar(50) NOT NULL,
  `perihal` varchar(70) NOT NULL,
  `dari` varchar(60) NOT NULL,
  `dokumen` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_masuk`
--

INSERT INTO `surat_masuk` (`nomer`, `perihal`, `dari`, `dokumen`) VALUES
('22', '22', '22', '32-99-1-PB.pdf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`npm`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
